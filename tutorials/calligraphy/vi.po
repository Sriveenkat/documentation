msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Calligraphy\n"
"POT-Creation-Date: 2023-07-14 16:46+0200\n"
"PO-Revision-Date: 2008-02-02 01:36+0700\n"
"Last-Translator: Nguyen Dinh Trung <nguyendinhtrung141@gmail.com>\n"
"Language-Team: VIETNAMESE <VI@li.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""
"Clytie Siddall <clytie@riverland.net.au>, 2006, 2007, 2008\n"
"Nguyễn Đình Trung <nguyendinhtrung141@gmail.com>, 2008"

#. (itstool) path: articleinfo/title
#: tutorial-calligraphy.xml:6
msgid "Calligraphy"
msgstr "Thư pháp"

#. (itstool) path: articleinfo/subtitle
#: tutorial-calligraphy.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-calligraphy.xml:11
msgid ""
"One of the many great tools available in Inkscape is the Calligraphy tool. "
"This tutorial will help you become acquainted with how that tool works, as "
"well as demonstrate some basic techniques of the art of Calligraphy."
msgstr ""
"Một trong những công cụ độc đáo mà Inkscape cung cấp cho bạn là công cụ Thư "
"pháp, công cụ tạo nét viết đẹp. Bài thực hành này sẽ hướng dẫn bạn làm quen "
"với cách thức làm việc với công cụ Thư pháp, cũng như một số kỹ thuật cơ bản "
"của thư pháp. "

#. (itstool) path: abstract/para
#: tutorial-calligraphy.xml:15
#, fuzzy
msgid ""
"Use <keycombo><keycap function=\"control\">Ctrl</keycap><keycap>arrows</"
"keycap></keycombo>, <mousebutton role=\"mouse-wheel\">mouse wheel</"
"mousebutton>, or <mousebutton role=\"middle-button-drag\">middle button "
"drag</mousebutton> to scroll the page down. For basics of object creation, "
"selection, and transformation, see the Basic tutorial in "
"<menuchoice><guimenu>Help</guimenu><guimenuitem>Tutorials</guimenuitem></"
"menuchoice>."
msgstr ""
"Dùng <keycap>Ctrl+mũi tên</keycap>, <keycap>xoay chuột</keycap>, hoặc "
"<keycap>kéo chuột giữa</keycap> để cuộn xuống dưới. Xem phần hướng dẫn Cơ "
"bản trong <command>Trợ giúp &gt; Hướng dẫn</command> để biết cách tạo các "
"đối tượng, vùng chọn và chuyển dạng đối tượng."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:23
msgid "History and Styles"
msgstr "Vài nét lịch sử về Thư pháp"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:24
msgid ""
"Going by the dictionary definition, <firstterm>calligraphy</firstterm> means "
"“beautiful writing” or “fair or elegant penmanship”. Essentially, "
"calligraphy is the art of making beautiful or elegant handwriting. It may "
"sound intimidating, but with a little practice, anyone can master the basics "
"of this art."
msgstr ""
"Trong từ điển, <firstterm>Thư pháp</firstterm> có nghĩa là thuật viết chữ "
"đẹp, nghệ thuật tạo ra chữ viết tay đẹp và thanh nhã. Nghe có vẻ phức tạp, "
"nhưng nếu bạn bỏ thời gian tập một chút, bạn có thể lãnh hội nghệ thuật này "
"ngay."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:29
#, fuzzy
msgid ""
"Up until roughly 1440 AD, before the printing press was around, calligraphy "
"was the way books and other publications were made. A scribe had to "
"handwrite every individual copy of every book or publication. Perhaps the "
"most common place where the average person will run across calligraphy today "
"is in the realm of motivating handlettering wall art, though."
msgstr ""
"Thư pháp đã xuất hiện ngay trong các bức vẽ trên vách hang từ thời cổ đại. "
"Cho tới năm 1440, khi mà chưa có công nghệ in sắp chữ, thư pháp là cách con "
"người dùng để trình bày sách và các giấy tờ công văn khác. Người thợ chép "
"sách đã phải viết tay từng bản sao một, bằng bút lông chim chấm vào mực, "
"viết lên giấy da. Các kiểu chữ thời đó bao gồm Rustic, Carolingian, "
"Blackletter... Ngày nay, Thư pháp vẫn còn được dùng để viết thiếp cưới, hoặc "
"trang trí biểu hình, biểu tượng, thương hiệu."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:35
msgid "There are three main styles of calligraphy:"
msgstr "Có 3 phong cách thư pháp chính:"

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:40
msgid ""
"Western or Roman: The preserved texts are mainly written with a quill and "
"ink onto materials such as parchment or vellum."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:46
msgid ""
"Arabic, including other languages in Arabic script, executed with a reed pen "
"and ink on paper. (This style has produced by far the largest bulk of "
"preserved texts before 1440.)"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:52
#, fuzzy
msgid "Chinese or Oriental, executed with a brush."
msgstr "Trung Quốc và các nước phương Đông"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:57
msgid ""
"The three styles have influenced each other on different occasions. Western "
"lettering styles used throughout the ages include Rustic, Carolingian, "
"Blackletter, etc."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:61
#, fuzzy
msgid ""
"Today, one great advantage that we have over the scribes of the past is the "
"<guimenuitem>Undo</guimenuitem> command: If you make a mistake, the entire "
"page is not ruined. Inkscape's Calligraphy tool also enables some techniques "
"which would not be possible with a traditional pen-and-ink."
msgstr ""
"Với Inkscape, bạn có thể dùng lệnh <command>Huỷ bước</command> ngay khi thực "
"hiện nhầm 1 thao tác nào đó. Và công cụ Thư pháp trong Inkscape còn cho phép "
"ta làm nhiều điều mà với giấy và bút lông chim, ta không bao giờ làm được."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:68
msgid "Hardware"
msgstr "Phần cứng"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:69
#, fuzzy
msgid ""
"You will get the best results if you use a <firstterm>tablet and pen</"
"firstterm>. Thanks to the flexibility of our tool, even those with only a "
"mouse can do some fairly intricate calligraphy, though there will be some "
"difficulty producing fast sweeping strokes."
msgstr ""
"Nếu có một <firstterm>bàn vẽ điện tử</firstterm> (e.g. Wacom), bạn sẽ dễ "
"dàng làm việc với công cụ Thư pháp hơn. Bạn cũng có thể dùng chuột, nhưng so "
"với bàn vẽ, chuột sẽ không thể linh hoạt và nhạy bằng."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:74
msgid ""
"Inkscape is able to utilize <firstterm>pressure sensitivity</firstterm> and "
"<firstterm> tilt sensitivity</firstterm> of a tablet pen that supports these "
"features. The sensitivity functions are disabled by default because they "
"require configuration. Also, keep in mind that calligraphy with a quill or "
"pen with nib are also not very sensitive to pressure, unlike a brush."
msgstr ""
"Inkscape cho phép sử dụng các chức năng <firstterm>cảm ứng lực ấn</"
"firstterm> và <firstterm>cảm ứng góc nghiêng</firstterm> của bàn vẽ điện tử, "
"nếu có. Tuy nhiên các chức năng này mặc định là không được sử dụng, vì chúng "
"ta phải cấu hình thiết bị trước khi dùng. Và bút lông chim hay bút mực "
"thường không bị ảnh hưởng nhiều bởi lực ấn bút, khác hẳn với bút lông dùng "
"trong thư pháp Trung Quốc."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:80
#, fuzzy
msgid ""
"If you have a tablet and would like to utilize the sensitivity features, you "
"will need to configure your device. This configuration will only need to be "
"performed once and the settings are saved. To enable this support you must "
"have the tablet plugged in prior to starting Inkscape and then proceed to "
"open the <guimenuitem>Input Devices...</guimenuitem> dialog through the "
"<guimenu>Edit</guimenu> menu. With this dialog open you can choose the "
"preferred device and settings for your tablet pen. Lastly, after choosing "
"those settings, switch to the Calligraphy tool and toggle the toolbar "
"buttons for pressure and tilt. From now on, Inkscape will remember those "
"settings on startup."
msgstr ""
"Nếu bạn có bàn vẽ điện tử và muốn dùng các chức năng cảm ứng độ nghiêng và "
"lực ấn bút, bạn sẽ phải cấu hình thiết bị này. Các cấu hình chỉ cần làm 1 "
"lần, và sẽ được lưu lại. Để bật chức năng hỗ trợ trong Inkscape, bạn phải "
"cắm bàn vẽ vào trước khi bật Inkscape, rồi mở phần <firstterm>Thiết bị "
"nhập...</firstterm> trong trình đơn <emphasis>Tập tin</emphasis>. Trong hộp "
"thoại này, bạn có thể chọn thiết bị mình muốn dùng, và đặt các thiết lập cho "
"bàn vẽ điện tử. Cuối cùng, sau khi chọn các thiết lập xong, hãy thử chuyển "
"sang công cụ Thư pháp và bật nút tương ứng trên thanh Điều khiển công cụ để "
"lấy thông số cảm ứng được về lực ấn bút và độ nghiêng. Sau đó, Inkscape sẽ "
"lưu lại các thiết lập và dùng chúng mỗi khi chạy."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:89
msgid ""
"The Inkscape calligraphy pen can be sensitive to the <firstterm>velocity</"
"firstterm> of the stroke (see “Thinning” below), so if you are using a "
"mouse, you'll probably want to zero this parameter."
msgstr ""
"Bút thư pháp của Inkscape cũng rất nhạy với <firstterm>tốc độ</firstterm> "
"nét vẽ được thực hiện (xem phần \"Thu hẹp\" ở dưới), nên nếu dùng chuột thì "
"bạn nên đặt tham số này là 0."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:95
msgid "Calligraphy Tool Options"
msgstr "Các tuỳ chọn cho công cụ Thư pháp"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:96
#, fuzzy
msgid ""
"Switch to the Calligraphy tool by pressing <keycombo><keycap "
"function=\"control\">Ctrl</keycap><keycap>F6</keycap></keycombo>, pressing "
"the <keycap>C</keycap> key, or by clicking on its toolbar button. On the top "
"toolbar, you will notice there are 8 options: <guilabel>Width</guilabel>, "
"<guilabel>Thinning</guilabel>, <guilabel>Mass</guilabel>, <guilabel>Angle</"
"guilabel>, <guilabel>Fixation</guilabel>, <guilabel>Caps</guilabel>, "
"<guilabel>Tremor</guilabel> and <guilabel>Wiggle</guilabel>. There are also "
"two buttons to toggle tablet Pressure and Tilt sensitivity on and off (for "
"drawing tablets), as well as a dropdown menu with a few presets to select "
"from, and the option to save your own presets."
msgstr ""
"Chọn công cụ Thư pháp bằng phím <keycap>Ctrl+F6</keycap>, nhấn phím "
"<keycap>C</keycap> hoặc bấm chuột vào nút tương ứng trên Hộp công cụ. Trên "
"thanh Điều khiển Công cụ, bạn sẽ thấy có 7 tham số: Rộng &amp; Thu hẹp; Góc "
"&amp; Độ cố định; Nét run; Khối lượng &amp; Bút Ngọ nguậy. Ngoài ra còn có 2 "
"nút để bật tắt cảm ứng lực ấn bút và góc nghiêng bút (cho bàn vẽ). "

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:108
msgid "Width &amp; Thinning"
msgstr "Rộng &amp; Thu hẹp"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:109
#, fuzzy
msgid ""
"This pair of options control the <firstterm>width</firstterm> of your pen. "
"The width can vary from 1 to 100 and (by default) is measured in units "
"relative to the size of your editing window, but independent of zoom "
"(symbolized as %). This makes sense, because the natural “unit of measure” "
"in calligraphy is the range of your hand's movement, and it is therefore "
"convenient to have the width of your pen nib in constant ratio to the size "
"of your “drawing board” and not in some real units which would make it "
"depend on zoom. This behavior is optional though, so it can be changed for "
"those who would prefer absolute units regardless of zoom. To switch the "
"unit, simply use the unit selector after the <guilabel>Unit</guilabel> field."
msgstr ""
"Hai tham số này cho phép bạn điều chỉnh <firstterm>độ rộng</firstterm> của "
"ngòi bút. Giá trị Rộng trong khoảng từ 1 đến 100 và (mặc định) được đo bằng "
"đơn vị tương đối so với kích cỡ của vùng vẽ, nhưng không phụ thuộc vào mức "
"độ thu phóng. Đó là do đơn vị đo đối với công cụ Thư pháp là phạm vi di "
"chuyển của tay bạn, nên sẽ thuận lợi hơn khi vẽ bằng bút có độ rộng nét tỉ "
"lệ không đổi với kích cỡ của bản vẽ, và không bị phụ thuộc vào độ thu phóng. "
"Tuy nhiên chế độ này có thể thay đổi được, nếu bạn bấm đúp chuột vào nút Thư "
"pháp trên thanh công cụ, và chọn ô <emphasis>Dùng đơn vị tuyệt đối</"
"emphasis> trong phần Tuỳ chỉnh cho công cụ Thư pháp."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:118
msgid ""
"Since pen width is changed often, you can adjust it without going to the "
"toolbar, using the <keycap>left</keycap> and <keycap>right</keycap> arrow "
"keys or with a tablet that supports the pressure sensitivity function. The "
"best thing about these keys is that they work while you are drawing, so you "
"can change the width of your pen gradually in the middle of the stroke:"
msgstr ""
"Độ rộng ngòi bút có thể thay đổi thông qua các phím mũi tên <keycap>trái</"
"keycap> và <keycap>phải</keycap> hoặc qua lực nhấn lên bàn vẽ nếu chức năng "
"cảm ứng lực ấn được bật, nên bạn có thể điều chỉnh nó mà không cần phải truy "
"cập vào Thanh điều khiển công cụ. Các phím này làm việc ngay khi bạn vẽ, nên "
"bạn có thể thay đổi độ rộng của nét vẽ từ từ trong lúc rê chuột:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:131
#, fuzzy
msgid ""
"Pen width may also depend on the velocity, as controlled by the "
"<firstterm>thinning</firstterm> parameter. This parameter can take values "
"from -100 to 100; zero means the width is independent of velocity, positive "
"values make faster strokes thinner, negative values make faster strokes "
"broader. The default of 10 means moderate thinning of fast strokes. Here are "
"a few examples, all drawn with width=20 and angle=90:"
msgstr ""
"Độ rộng ngòi bút cũng phụ thuộc vào tốc độ vẽ của bạn, và tham số "
"<firstterm>Thu hẹp</firstterm> điều khiển tốc độ thay đổi. Giá trị của ô Thu "
"hẹp là từ -1 đến 1; giá trị 0 tương ứng với việc nét bút không phụ thuộc vào "
"vận tốc, giá trị dương làm cho nét bút mảnh hơn khi di chuột nhanh, tham số "
"âm thì ngược lại. Dưới đây là một vài thí dụ, được vẽ với thông số Rộng=20 "
"và Góc=90:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:144
#, fuzzy
msgid ""
"For fun, set Width and Thinning both to 100 (maximum) and draw with jerky "
"movements to get strangely naturalistic, neuron-like shapes:"
msgstr ""
"Hãy thử đặt giá trị Rộng =100 và Thu hẹp=1 (lớn nhất) rồi vẽ các nét nguệch "
"ngoạc xem sao:"

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:157
msgid "Angle &amp; Fixation"
msgstr "Góc &amp; Độ cố định"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:158
msgid ""
"After width, <firstterm>angle</firstterm> is the most important calligraphy "
"parameter. It is the angle of your pen in degrees, changing from 0 "
"(horizontal) to 90 (vertical counterclockwise) or to -90 (vertical "
"clockwise). Note that if you turn tilt sensitivity on for a tablet, the "
"angle parameter is greyed out and the angle is determined by the tilt of the "
"pen."
msgstr ""
"Sau khi điều chỉnh độ rộng ngòi bút, <firstterm>góc di bút</firstterm> cũng "
"là một tham số quan trọng trong thư pháp. Giá trị Góc được tính bằng độ: 0 "
"độ tương ứng với khi ngòi bút nằm ngang, 90 độ tương ứng với khi ngòi bút "
"nằm dọc ngược chiều kim đồng hồ và -90 độ tương ứng với khi ngòi bút nằm dọc "
"theo chiều kim đồng hồ. Lưu ý là nếu bạn bật cảm ứng độ nghiêng của bàn vẽ "
"lên, tham số Góc sẽ bị bỏ đi và bạn chỉ có thể dùng bút trên bàn vẽ điện tử "
"để thay đổi góc nghiêng này."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:171
#, fuzzy
msgid ""
"Each traditional calligraphy style has its own prevalent pen angle. For "
"example, the Uncial hand uses the angle of 25 degrees. More complex hands "
"and more experienced calligraphers will often vary the angle while drawing, "
"and Inkscape makes this possible by pressing <keycap>up</keycap> and "
"<keycap>down</keycap> arrow keys or with a tablet that supports the tilt "
"sensitivity feature. For beginning calligraphy lessons, however, keeping the "
"angle constant will work best. Here are examples of strokes drawn at "
"different angles (fixation = 100):"
msgstr ""
"Mỗi phong cách thư pháp đều có góc đặt bút chuẩn. Ví dụ, phong cách Unicial "
"dùng góc bút 25 độ. Bạn có thể thay đổi góc bút bằng phím mũi tên "
"<keycap>lên</keycap><keycap>xuống</keycap> hoặc góc nghiêng của bút điện tử "
"(nếu chức năng cảm ứng góc nghiêng được bật) trong khi vẽ. Tuy nhiên, nếu "
"bạn mới làm quen với Thư pháp thì trước hết hãy giữ cho góc nghiêng bút là "
"cố định. Dưới đây là một số thí dụ khi vẽ các nét bút theo góc nghiêng cố "
"định (Độ cố định=1):"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:185
msgid ""
"As you can see, the stroke is at its thinnest when it is drawn parallel to "
"its angle, and at its broadest when drawn perpendicular. Positive angles are "
"the most natural and traditional for right-handed calligraphy."
msgstr ""
"Như bạn thấy, nét bút trở nên mảnh nhất khi được vẽ song song với Góc đặt "
"bút, và rộng nhất khi vẽ vuông góc với Góc đặt bút. Các góc dương thường cho "
"cảm giác tự nhiên, mô phỏng nét chữ viết bằng tay phải."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:189
#, fuzzy
msgid ""
"The level of contrast between the thinnest and the thickest is controlled by "
"the <firstterm>fixation</firstterm> parameter. The value of 100 means that "
"the angle is always constant, as set in the Angle field. Decreasing fixation "
"lets the pen turn a little against the direction of the stroke. With "
"fixation=0, pen rotates freely to be always perpendicular to the stroke, and "
"Angle has no effect anymore:"
msgstr ""
"Mức độ tương phản giữa nét đậm và nét nhạt được điều chỉnh bởi tham số "
"<firstterm>Độ cố định</firstterm>. Đây là giá trị tương ứng với sự thay đổi "
"của góc đặt bút trong khi vẽ. Giá trị này bằng 1 có nghĩa là góc nghiêng bút "
"luôn không đổi, và chính bằng góc được đặt trong ô Góc. Với giá trị 0, ngòi "
"bút của bạn sẽ xoay tự do để luôn luôn vuông góc với nét, và giá trị đặt "
"trên tham số Góc sẽ bị vô hiệu hóa: "

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:202
msgid ""
"Typographically speaking, maximum fixation and therefore maximum stroke "
"width contrast (above left) are the features of antique serif typefaces, "
"such as Times or Bodoni (because these typefaces were historically an "
"imitation of fixed-pen calligraphy). Zero fixation and zero width contrast "
"(above right), on the other hand, suggest modern sans serif typefaces such "
"as Helvetica."
msgstr ""
"Khi bạn đặt Độ cố định bằng 1, bạn sẽ thu được các mặt chữ cổ như Times hoặc "
"Bodoni, giống như phần bên trái hình trên (vì chúng là các mặt chữ mô phỏng "
"thư pháp được tạo ra với nét bút không đổi). Khi đặt Fixation bằng 0, ta thu "
"được các mặt chữ hiện đại như Sans Serif hoặc Helvetica, giống phần bên phải."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:210
msgid "Tremor"
msgstr "Nét run:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:211
#, fuzzy
msgid ""
"<firstterm>Tremor</firstterm> is intended to give a more natural look to the "
"calligraphy strokes. Tremor is adjustable in the Controls bar with values "
"ranging from 0 to 100. It will affect your strokes producing anything from "
"slight unevenness to wild blotches and splotches. This significantly expands "
"the creative range of the tool."
msgstr ""
"<firstterm>Nét run</firstterm> được dùng để tạo nét bút trông tự nhiên hơn. "
"Ta có thể chọn Nét run từ 0.0 đến 1.0 trên thanh Điều khiển Công cụ. Tham số "
"này quyết định mức độ run rẩy của nét vẽ, làm công cụ Thư pháp càng hữu dụng "
"hơn."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:226
msgid "Wiggle &amp; Mass"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:227
msgid ""
"Unlike width and angle, these two last parameters define how the tool "
"“feels” rather than affect its visual output. So there won't be any "
"illustrations in this section; instead just try them yourself to get a "
"better idea."
msgstr ""
"Không giống các tham số về độ rộng và góc ở phần trước, 2 tham số này định "
"nghĩa cách thức công cụ Thư pháp “cảm nhận” hơn là vẽ ra một thứ gì trên tài "
"liệu của bạn. Cho nên trong phần này sẽ không có hình minh họa nào cả, và "
"bạn sẽ phải thử thực hành nhiều lần để hiểu hơn về chúng."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:232
#, fuzzy
msgid ""
"<firstterm>Wiggle</firstterm> is the resistance of the paper to the movement "
"of the pen. The default is at minimum (0), and increasing this parameter "
"makes paper “slippery”: if the mass is big, the pen tends to run away on "
"sharp turns; if the mass is zero, high wiggle makes the pen to wiggle wildly."
msgstr ""
"<firstterm>Ngọ nguậy</firstterm> là tham số tương ứng với độ ma sát giữa "
"giấy và ngòi bút. Giá trị mặc định là 1, và nếu bạn đặt giá trị này thấp hơn "
"thì tớ giấy sẽ “trơn” hơn: nếu giá trị Khối lượng lớn thì ngòi bút hay bị "
"trượt ra ngoài khi vẽ các nét ngoắt ngoéo; nếu giá trị Khối lượng là 0 thì "
"giá trị Ngọ nguậy thấp sẽ khiến cho ngòi bút lắc mạnh."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:237
#, fuzzy
msgid ""
"In physics, <firstterm>mass</firstterm> is what causes inertia; the higher "
"the mass of the Inkscape calligraphy tool, the more it lags behind your "
"mouse pointer and the more it smoothes out sharp turns and quick jerks in "
"your stroke. By default this value is quite small (2) so that the tool is "
"fast and responsive, but you can increase mass to get slower and smoother "
"pen."
msgstr ""
"Trong thực tế, <firstterm>Khối lượng</firstterm> gây ra quán tính; khối "
"lượng càng lớn thì quán tính càng lớn, nên nét vẽ sẽ có 1 đoạn “đuôi” ở phía "
"sau điểm bạn uốn nét. Giá trị mặc định ở đây là (0.02), nên công cụ Thư pháp "
"mặc định là có đáp ứng nhanh, nhưng bạn có thể tăng nó lên để nét vẽ chậm và "
"mượt mà hơn."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:245
msgid "Calligraphy examples"
msgstr "Các ví dụ về Thư pháp"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:246
msgid ""
"Now that you know the basic capabilities of the tool, you can try to produce "
"some real calligraphy. If you are new to this art, get yourself a good "
"calligraphy book and study it with Inkscape. This section will show you just "
"a few simple examples."
msgstr ""
"Sau khi đã hiểu sơ sơ về các khả năng của công cụ Thư pháp, bạn hãy dùng nó "
"để thử tạo ra một tác phẩm thư pháp xem sao! Nếu bạn chưa bao giờ tập vẽ thư "
"pháp, hãy kiếm cho mình một cuốn sách hay về nghệ thuật này và học nó trên "
"Inkscape. Chương này cho bạn xem một số ví dụ về thư pháp."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:251
msgid ""
"First of all, to do letters, you need to create a pair of rulers to guide "
"you. If you're going to write in a slanted or cursive hand, add some slanted "
"guides across the two rulers as well, for example:"
msgstr ""
"Trước hết, để viết một lá thư, bạn cần phải tạo ra một cặp thước kẻ để định "
"vị nét bút. Nếu bạn viết chữ nghiêng hay chữ thảo, hãy thêm một số đường "
"gióng nghiêng giữa 2 thước kẻ. Ví dụ:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:262
msgid ""
"Then zoom in so that the height between the rulers corresponds to your most "
"natural hand movement range, adjust width and angle, and off you go!"
msgstr ""
"Sau đó phóng to để sao cho chiều cao giữa 2 thước kẻ tương ứng với độ di "
"chuyển cổ tay của bạn, rồi đặt các tham số độ rộng và góc trước khi viết!"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:266
#, fuzzy
msgid ""
"Probably the first thing you would do as a beginner calligrapher is practice "
"the basic elements of letters — vertical and horizontal stems, round "
"strokes, slanted stems. Here are some letter elements for the Uncial hand:"
msgstr ""
"Trước hết bạn phải tập các yếu tố cơ bản của vị trí dọc và ngang thân chữ, "
"làm tròn nét, thân chữ nghiêng. Dưới đây là một số yếu tố của chữ viết kiểu "
"Unicial: "

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:278
msgid "Several useful tips:"
msgstr "Một số mẹo nhỏ:"

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:283
#, fuzzy
msgid ""
"If your hand is comfortable on the tablet, don't move it. Instead, scroll "
"the canvas (<keycombo><keycap function=\"control\">Ctrl</"
"keycap><keycap>arrow</keycap></keycombo> keys) with your left hand after "
"finishing each letter."
msgstr ""
"Nếu tay bạn đặt thoải mái trên bàn viết, đừng di chuyển nó. Thay vào đó, hãy "
"cuộn khung vẽ (<keycap>Ctrl+mũi tên</keycap>) bằng tay trái sau khi hoàn tất "
"mỗi chữ."

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:290
#, fuzzy
msgid ""
"If your last stroke is bad, just undo it (<keycombo><keycap "
"function=\"control\">Ctrl</keycap><keycap>Z</keycap></keycombo>). However, "
"if its shape is good but the position or size are slightly off, it's better "
"to switch to Selector temporarily (<keycap>Space</keycap>) and nudge/scale/"
"rotate it as needed (using <mousebutton>mouse</mousebutton> or keys), then "
"press <keycap>Space</keycap> again to return to Calligraphy tool."
msgstr ""
"Nếu nét bút cuối cùng của bạn trông xấu, hãy sửa lại bằng lệnh Hủy bước "
"(<keycap>Ctrl+Z</keycap>). Tuy nhiên, nếu hình dạng của nó đạt chất lượng "
"nhưng vị trí cũng như kích thước hơi bị sai, hãy chuyển sang công cụ Chọn "
"bằng <keycap>phím cách</keycap> và di chuyển, co giãn hay xoay lại nét vẽ "
"cho vừa ý. Sau đó nhấn <keycap>phím cách</keycap> thêm lần nữa để chuyển lại "
"về công cụ Thư pháp."

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:299
msgid ""
"Having done a word, switch to Selector again to adjust stem uniformity and "
"letterspacing. Don't overdo this, however; good calligraphy must retain "
"somewhat irregular handwritten look. Resist the temptation to copy over "
"letters and letter elements; each stroke must be original."
msgstr ""
"Sau khi hoàn tất một từ, hãy chuyển sang công cụ Chọn và điều chỉnh lại cho "
"phù hợp. Đừng làm quá, vì các bản thư pháp đẹp phải giữ lại tính bất quy tắc "
"của nét vẽ tay. Tránh vội vàng sao chép các chữ và các thành phần của chữ: "
"hãy vẽ những nét nguyên bản."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:306
msgid "And here are some complete lettering examples:"
msgstr "Và đây là một số ví dụ hoàn chỉnh về Thư pháp:"

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:318
msgid "Conclusion"
msgstr "Kết luận"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:319
msgid ""
"Calligraphy is not only fun; it's a deeply spiritual art that may transform "
"your outlook on everything you do and see. Inkscape's calligraphy tool can "
"only serve as a modest introduction. And yet it is very nice to play with "
"and may be useful in real design. Enjoy!"
msgstr ""
"Thư pháp là một công cụ rất lý thú; nó dùng để diễn tả cả một nghệ thuật, có "
"thể được áp dụng cho tất cả các sáng tạo của bạn. Hãy học cách dùng Thư pháp "
"và ứng dụng nó một cách hiệu quả trong các ảnh mà bạn thiết kế. Enjoy!"

#. (itstool) path: Work/format
#: calligraphy-f01.svg:48 calligraphy-f02.svg:48 calligraphy-f03.svg:48
#: calligraphy-f04.svg:80 calligraphy-f05.svg:48 calligraphy-f06.svg:64
#: calligraphy-f07.svg:49 calligraphy-f08.svg:48 calligraphy-f09.svg:48
#: calligraphy-f10.svg:48
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. (itstool) path: text/tspan
#: calligraphy-f01.svg:69
#, fuzzy, no-wrap
msgid "width=1, growing....                       reaching 47, decreasing...                                  back to 0"
msgstr "Rộng=0.01, tăng dần....                       đến 0.47, giảm dần...                                  trở về 0"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:69
#, no-wrap
msgid "thinning = 0 (uniform width) "
msgstr "Thu hẹp = 0 (chiều rộng cố định)"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:80
#, fuzzy, no-wrap
msgid "thinning = 10"
msgstr "Thu hẹp = 0.1"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:91
#, fuzzy, no-wrap
msgid "thinning = 40"
msgstr "Thu hẹp = 0.1"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:102
#, fuzzy, no-wrap
msgid "thinning = −20"
msgstr "Thu hẹp = -0.2"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:113
#, fuzzy, no-wrap
msgid "thinning = −60"
msgstr "Thu hẹp = -0.2"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:120
#, no-wrap
msgid "angle = 90 deg"
msgstr "Góc = 90 độ"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:131
#, no-wrap
msgid "angle = 30 (default)"
msgstr "Góc = 30 (mặc định)"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:142 calligraphy-f05.svg:102
#, no-wrap
msgid "angle = 0"
msgstr "Góc = 0"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:153
#, fuzzy, no-wrap
msgid "angle = −90 deg"
msgstr "Góc = 90 độ"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:69 calligraphy-f06.svg:85 calligraphy-f06.svg:101
#: calligraphy-f06.svg:117
#, no-wrap
msgid "angle = 30"
msgstr "Góc = 30"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:80
#, no-wrap
msgid "angle = 60"
msgstr "Góc = 60"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:91
#, no-wrap
msgid "angle = 90"
msgstr "Góc = 90"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:113
#, no-wrap
msgid "angle = 15"
msgstr "Góc = 15"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:124
#, fuzzy, no-wrap
msgid "angle = −45"
msgstr "Góc = -45"

#. (itstool) path: text/tspan
#: calligraphy-f06.svg:90
#, fuzzy, no-wrap
msgid "fixation = 100"
msgstr "Độ cố định = 1"

#. (itstool) path: text/tspan
#: calligraphy-f06.svg:106
#, fuzzy, no-wrap
msgid "fixation = 80"
msgstr "Độ cố định = 0"

#. (itstool) path: text/tspan
#: calligraphy-f06.svg:122
#, no-wrap
msgid "fixation = 0"
msgstr "Độ cố định = 0"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:73
#, no-wrap
msgid "slow"
msgstr "chậm"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:84
#, no-wrap
msgid "medium"
msgstr "vừa"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:95
#, no-wrap
msgid "fast"
msgstr "nhanh"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:179
#, no-wrap
msgid "tremor = 0"
msgstr "Nét run = 0"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:190
#, fuzzy, no-wrap
msgid "tremor = 10"
msgstr "Nét run = 1.0"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:201
#, fuzzy, no-wrap
msgid "tremor = 30"
msgstr "Nét run = 0"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:212
#, fuzzy, no-wrap
msgid "tremor = 50"
msgstr "Nét run = 0"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:223
#, fuzzy, no-wrap
msgid "tremor = 70"
msgstr "Nét run = 0"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:234
#, fuzzy, no-wrap
msgid "tremor = 90"
msgstr "Nét run = 0"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:245
#, fuzzy, no-wrap
msgid "tremor = 20"
msgstr "Nét run = 0"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:256
#, fuzzy, no-wrap
msgid "tremor = 40"
msgstr "Nét run = 0"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:267
#, fuzzy, no-wrap
msgid "tremor = 60"
msgstr "Nét run = 0"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:278
#, fuzzy, no-wrap
msgid "tremor = 80"
msgstr "Nét run = 0"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:289
#, fuzzy, no-wrap
msgid "tremor = 100"
msgstr "Nét run = 1.0"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:117
#, no-wrap
msgid "Uncial hand"
msgstr "chữ Uncial"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:128
#, no-wrap
msgid "Carolingian hand"
msgstr "chữ Carolingian"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:139
#, no-wrap
msgid "Gothic hand"
msgstr "chữ Gothic"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:150
#, no-wrap
msgid "Bâtarde hand"
msgstr "chữ Bâtarde"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:175
#, no-wrap
msgid "Flourished Italic hand"
msgstr "chữ nghiêng trang trí"

#~ msgid "Western or Roman"
#~ msgstr "Châu Âu và La Mã"

#~ msgid "Arabic"
#~ msgstr "A-rập"

#~ msgid ""
#~ "This tutorial focuses mainly on Western calligraphy, as the other two "
#~ "styles tend to use a brush (instead of a pen with nib), which is not how "
#~ "our Calligraphy tool currently functions."
#~ msgstr ""
#~ "Công cụ Thư pháp trong Inkscape mô phỏng thư pháp Châu Âu và La Mã, sử "
#~ "dụng bút có ngòi nhọn như bút mực hoặc bút lông chim. Công cụ Thư pháp "
#~ "còn cho phép ta thực hiện một số kỹ thuật mà ta sẽ chẳng bao giờ làm được "
#~ "bằng bút và mực."

#~ msgid "angle = -90 deg"
#~ msgstr "Góc = -90 độ"

#~ msgid ""
#~ "bulia byak, buliabyak@users.sf.net and josh andler, scislac@users.sf.net"
#~ msgstr ""
#~ "bulia byak, buliabyak@users.sf.net and josh andler, scislac@users.sf.net"

#~ msgid "Mass &amp; Drag"
#~ msgstr "Khối lượng bút &amp; Bút ngọ nguậy"

#~ msgid "thinning = 0.4"
#~ msgstr "Thu hẹp = 0.4"

#~ msgid "thinning = -0.6"
#~ msgstr "Thu hẹp = -0.6"

#~ msgid "fixation = 0.8"
#~ msgstr "Độ cố định = 0.8"

#~ msgid "tremor = 0.1"
#~ msgstr "Nét run = 0.1"

#~ msgid "tremor = 0.3"
#~ msgstr "Nét run = 0.3"

#~ msgid "tremor = 0.5"
#~ msgstr "Nét run = 0.5"

#~ msgid "tremor = 0.7"
#~ msgstr "Nét run = 0.7"

#~ msgid "tremor = 0.9"
#~ msgstr "Nét run = 0.9"

#~ msgid "tremor = 0.2"
#~ msgstr "Nét run = 0.2"

#~ msgid "tremor = 0.4"
#~ msgstr "Nét run = 0.4"

#~ msgid "tremor = 0.6"
#~ msgstr "Nét run = 0.6"

#~ msgid "tremor = 0.8"
#~ msgstr "Nét run = 0.8"
