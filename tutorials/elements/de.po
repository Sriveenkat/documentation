# Translation of Inkscape's elements tutorial to German.
#
#
# Uwe Schöler <uschoeler@yahoo.de>, 2006.
# Colin Marquardt <colin@marquardt-home.de>, 2006.
# Maren Hachmann <marenhachmann@yahoo.com>, 2016, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Elements of Design\n"
"POT-Creation-Date: 2023-07-14 16:46+0200\n"
"PO-Revision-Date: 2023-06-23 17:42+0200\n"
"Last-Translator: Maren Hachmann <marenhachmann@yahoo.com>\n"
"Language-Team: German <de@li.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.3\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""
"Uwe Schöler <uschoeler@yahoo.de>, 2006\n"
"Colin Marquardt <colin@marquardt-home.de>, 2006\n"
"Maren Hachmann <marenhachmann@yahoo.com>, 2015, 2023"

#. (itstool) path: articleinfo/title
#: tutorial-elements.xml:6
msgid "Elements of design"
msgstr "Elemente des Designs"

#. (itstool) path: articleinfo/subtitle
#: tutorial-elements.xml:7
msgid "Tutorial"
msgstr "Tutorial"

#. (itstool) path: abstract/para
#: tutorial-elements.xml:11
msgid ""
"This tutorial will demonstrate the elements and principles of design which "
"are normally taught to early art students in order to understand various "
"properties used in art making. This is not an exhaustive list, so please "
"add, subtract, and combine to make this tutorial more comprehensive."
msgstr ""
"Dieses Tutorial demonstriert Elemente und Prinzipien des Designs. Diese "
"Eigenschaften der Erschaffung von Kunst werden Kunststudenten normalerweise "
"frühzeitig beigebracht. Dies ist keine vollständige Liste - wer mag, kann "
"dabei helfen, weitere Informationen hinzuzufügen, um diese Einführung weiter "
"zu verbessern."

#. (itstool) path: sect1/title
#: tutorial-elements.xml:25
msgid "Elements of Design"
msgstr "Elemente des Designs"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:26
msgid "The following elements are the building blocks of design."
msgstr "Die folgenden Elemente sind die Grundelemente eines Designs."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:30 elements-f01.svg:132
#, no-wrap
msgid "Line"
msgstr "Linie"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:31
msgid ""
"A line is defined as a mark with length and direction, created by a point "
"that moves across a surface. A line can vary in length, width, direction, "
"curvature, and color. Line can be two-dimensional (a pencil line on paper), "
"or implied three-dimensional."
msgstr ""
"Eine Linie ist als ein Gebilde mit Länge und Richtung definiert, erzeugt "
"durch einen Punkt, der sich über eine Oberfläche bewegt. Eine Linie kann in "
"Länge, Breite, Richtung, Krümmung und Farbe variieren. Linien können "
"zweidimensional (ein Bleistiftstrich auf Papier), oder impliziert "
"dreidimensional sein."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:45 elements-f01.svg:145
#, no-wrap
msgid "Shape"
msgstr "Form"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:46
msgid ""
"A flat figure, shape is created when actual or implied lines meet to "
"surround a space. A change in color or shading can define a shape. Shapes "
"can be divided into several types: geometric (square, triangle, circle) and "
"organic (irregular in outline)."
msgstr ""
"Eine Form ist ein flaches Gebilde und entsteht, wenn tatsächliche oder "
"implizierte Linien aufeinandertreffen und einen Freiraum umschließen. Ein "
"Wechsel in der Farbe oder Schattierung kann eine Form definieren. Formen "
"können in verschiedene Typen unterteilt werden: geometrische (Quadrat, "
"Dreieck, Kreis) und organische (unregelmäßig im Umriss)."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:60 elements-f01.svg:197
#, no-wrap
msgid "Size"
msgstr "Größe"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:61
msgid ""
"This refers to variations in the proportions of objects, lines or shapes. "
"There is a variation of sizes in objects either real or imagined."
msgstr ""
"Dieser Abschnitt bezieht sich auf Änderungen in den Proportionen von "
"Objekten, Linien oder Formen. Objekte können unterschiedlich groß sein oder "
"erscheinen."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:74 elements-f01.svg:158
#, no-wrap
msgid "Space"
msgstr "Raum"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:75
msgid ""
"Space is the empty or open area between, around, above, below, or within "
"objects. Shapes and forms are made by the space around and within them. "
"Space is often called three-dimensional or two- dimensional. Positive space "
"is filled by a shape or form. Negative space surrounds a shape or form."
msgstr ""
"Der Raum ist die Leere oder das offene Gebiet zwischen, um, über, unter, "
"oder innerhalb von Objekten. Formen bestehen aus dem Raum um sie herum und "
"in ihnen. Der Raum wird oft als zwei- oder dreidimensional bezeichnet. "
"Positiver Raum ist durch eine Form gefüllt. Negativer Raum umschließt eine "
"Form."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:89 elements-f01.svg:119
#, no-wrap
msgid "Color"
msgstr "Farbe"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:90
msgid ""
"Color is the perceived character of a surface according to the wavelength of "
"light reflected from it. Color has three dimensions: HUE (another word for "
"color, indicated by its name such as red or yellow), VALUE (its lightness or "
"darkness), INTENSITY (its brightness or dullness)."
msgstr ""
"Farbe ist der wahrgenommene Charakter einer Oberfläche entsprechend der "
"Wellenlänge des Lichts, welches davon reflektiert wird. Farbe hat drei "
"Dimensionen: FARBTON (ein anderes Wort für Farbe, bezeichnet durch deren "
"Name, wie rot oder gelb), WERT (ihre Helligkeit oder Dunkelheit), INTENSITÄT "
"(ihre Stärke oder Schwäche)."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:104 elements-f01.svg:171
#, no-wrap
msgid "Texture"
msgstr "Textur"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:105
msgid ""
"Texture is the way a surface feels (actual texture) or how it may look "
"(implied texture). Textures are described by word such as rough, silky, or "
"pebbly."
msgstr ""
"Textur ist die Art und Weise, wie sich eine Oberfläche anfühlt (tatsächliche "
"Textur) oder wie sie aussieht (implizierte Textur). Texturen werden durch "
"Worte wie rau, seidig oder kiesig beschrieben."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:118 elements-f01.svg:184
#, no-wrap
msgid "Value"
msgstr "Wert"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:119
msgid ""
"Value is how dark or how light something looks. We achieve value changes in "
"color by adding black or white to the color. Chiaroscuro uses value in "
"drawing by dramatically contrasting lights and darks in a composition."
msgstr ""
"Wert bedeutet, wie dunkel oder hell etwas aussieht. Man erreicht "
"Wertänderungen bei Farben durch Hinzufügen von Schwarz oder Weiß zur Farbe. "
"Chiaroscuro-Malerei nutzt Farbwerte in Zeichnungen durch dramatische "
"Kontrastierung von Lichtern und Schatten in einer Komposition."

#. (itstool) path: sect1/title
#: tutorial-elements.xml:133
msgid "Principles of Design"
msgstr "Designgrundlagen"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:134
msgid "The principles use the elements of design to create a composition."
msgstr ""
"Die Grundlagen benutzen die Designelemente, um eine Komposition zu erstellen."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:138 elements-f01.svg:210
#, no-wrap
msgid "Balance"
msgstr "Ausgeglichenheit"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:139
msgid ""
"Balance is a feeling of visual equality in shape, form, value, color, etc. "
"Balance can be symmetrical or evenly balanced or asymmetrical and un-evenly "
"balanced. Objects, values, colors, textures, shapes, forms, etc., can be "
"used in creating a balance in a composition."
msgstr ""
"Ausgeglichenheit (Balance) ist das Gefühl von visueller Gleichheit in Form, "
"Wert, Farbe, usw. Die Ausgeglichenheit kann symmetrisch und gleichmäßig "
"sein, oder asymmetrisch und ungleichmäßig. Objekte, Werte, Farben, Texturen, "
"Gebilde, Formen, usw. können zum Herstellen einer Ausgeglichenheit in einer "
"Komposition benutzt werden."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:153 elements-f01.svg:223
#, no-wrap
msgid "Contrast"
msgstr "Kontrast"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:154
msgid "Contrast is the juxtaposition of opposing elements"
msgstr "Kontrast ist das Nebeneinanderstellen von entgegengesetzten Elementen."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:166 elements-f01.svg:236
#, no-wrap
msgid "Emphasis"
msgstr "Betonung"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:167
msgid ""
"Emphasis is used to make certain parts of their artwork stand out and grab "
"your attention. The center of interest or focal point is the place a work "
"draws your eye to first."
msgstr ""
"Betonung wird benutzt, um bestimmte Teile eines Kunstwerkes herauszustellen, "
"um Ihre Aufmerksamkeit darauf zu lenken. Das Zentrum des Interesses oder der "
"Brennpunkt ist der Platz, wo die Augen zuerst hinschauen."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:180 elements-f01.svg:249
#, no-wrap
msgid "Proportion"
msgstr "Proportion"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:181
msgid ""
"Proportion describes the size, location or amount of one thing compared to "
"another."
msgstr ""
"Proportion beschreibt die Größe, Lage oder Anzahl eines Gegenstandes im "
"Vergleich zu einem anderen."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:193 elements-f01.svg:262
#, no-wrap
msgid "Pattern"
msgstr "Muster"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:194
msgid ""
"Pattern is created by repeating an element (line, shape or color) over and "
"over again."
msgstr ""
"Ein Muster wird erzeugt, indem ein Element (Line, Gebilde oder Farbe) immer "
"und immer wiederholt wird."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:206 elements-f01.svg:276
#, no-wrap
msgid "Gradation"
msgstr "Abstufung"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:207
msgid ""
"Gradation of size and direction produce linear perspective. Gradation of "
"color from warm to cool and tone from dark to light produce aerial "
"perspective. Gradation can add interest and movement to a shape. A gradation "
"from dark to light will cause the eye to move along a shape."
msgstr ""
"Eine Abstufung von Größe und Richtung erzeugt eine lineare Perspektive. "
"Farbabstufungen von warm nach kalt und Tonwertabstufungen von dunkel nach "
"hell erzeugt eine atmosphärische Perspektive. Abstufungen können einem "
"Gebilde Bedeutung und Bewegung verleihen. Eine Abstufung von dunkel nach "
"hell wird das Auge entlang eines Gebildes führen."

#. (itstool) path: sect1/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:222 elements-f01.svg:289
#, no-wrap
msgid "Composition"
msgstr "Komposition"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:223
msgid "The combining of distinct elements to form a whole."
msgstr "Die Kombination von abgegrenzten Elementen, um ein Ganzes zu formen."

#. (itstool) path: sect1/title
#: tutorial-elements.xml:235
msgid "Bibliography"
msgstr "Literaturverzeichnis"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:236
msgid ""
"This is a partial bibliography used to build this document (links may no "
"longer work today or link to malicious sites now, click with care)."
msgstr ""
"Dies ist eine anteilige Liste aller Quellen, die zur Erstellung dieses "
"Dokuments verwendet wurden (die Links funktionieren heute möglicherweise "
"nicht mehr oder verlinken auf gefährliche Webseiten, bitte nur mit Vorsicht "
"anklicken)."

#. (itstool) path: listitem/para
#: tutorial-elements.xml:242
msgid ""
"<ulink url=\"http://www.makart.com/resources/artclass/EPlist.html\">http://"
"www.makart.com/resources/artclass/EPlist.html</ulink>"
msgstr ""
"<ulink url=\"http://www.makart.com/resources/artclass/EPlist.html\">http://"
"www.makart.com/resources/artclass/EPlist.html</ulink>"

#. (itstool) path: listitem/para
#: tutorial-elements.xml:247
msgid ""
"<ulink url=\"http://www.princetonol.com/groups/iad/Files/elements2."
"htm\">http://www.princetonol.com/groups/iad/Files/elements2.htm</ulink>"
msgstr ""
"<ulink url=\"http://www.princetonol.com/groups/iad/Files/elements2."
"htm\">http://www.princetonol.com/groups/iad/Files/elements2.htm</ulink>"

#. (itstool) path: listitem/para
#: tutorial-elements.xml:252
msgid ""
"<ulink url=\"http://www.johnlovett.com/test.htm\">http://www.johnlovett.com/"
"test.htm</ulink>"
msgstr ""
"<ulink url=\"http://www.johnlovett.com/test.htm\">http://www.johnlovett.com/"
"test.htm</ulink>"

#. (itstool) path: listitem/para
#: tutorial-elements.xml:257
msgid ""
"<ulink url=\"http://digital-web.com/articles/elements_of_design/\">http://"
"digital-web.com/articles/elements_of_design/</ulink>"
msgstr ""
"<ulink url=\"http://digital-web.com/articles/elements_of_design/\">http://"
"digital-web.com/articles/elements_of_design/</ulink>"

#. (itstool) path: listitem/para
#: tutorial-elements.xml:262
msgid ""
"<ulink url=\"http://digital-web.com/articles/principles_of_design/\">http://"
"digital-web.com/articles/principles_of_design/</ulink>"
msgstr ""
"<ulink url=\"http://digital-web.com/articles/principles_of_design/\">http://"
"digital-web.com/articles/principles_of_design/</ulink>"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:267
msgid ""
"Special thanks to Linda Kim (<ulink url=\"http://www.coroflot.com/redlucite/"
"\">http://www.coroflot.com/redlucite/</ulink>) for helping me (<ulink "
"url=\"http://www.rejon.org/\">http://www.rejon.org/</ulink>) with this "
"tutorial. Also, thanks to the Open Clip Art Library (<ulink url=\"https://"
"openclipart.org/\">https://openclipart.org/</ulink>) and the graphics people "
"have submitted to that project."
msgstr ""
"Besonderer Dank an Linda Kim (<ulink url=\"http://www.coroflot.com/redlucite/"
"\">http://www.coroflot.com/redlucite/</ulink>) dafür, dass sie mir (<ulink "
"url=\"http://www.rejon.org/\">http://www.rejon.org/</ulink>) bei diesem "
"Tutorial geholfen hat. Dank auch an die Open Clip Art Library (<ulink "
"url=\"https://openclipart.org/\">https://openclipart.org/</ulink>) und die "
"Leute, die Grafiken zu diesem Projekt beigesteuert haben."

#. (itstool) path: Work/format
#: elements-f01.svg:51 elements-f02.svg:48 elements-f03.svg:48
#: elements-f04.svg:48 elements-f05.svg:48 elements-f06.svg:91
#: elements-f07.svg:91 elements-f08.svg:206 elements-f09.svg:48
#: elements-f10.svg:48 elements-f11.svg:48 elements-f12.svg:445
#: elements-f13.svg:102 elements-f14.svg:173 elements-f15.svg:452
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. (itstool) path: text/tspan
#: elements-f01.svg:92
#, no-wrap
msgid "Elements"
msgstr "Elemente"

#. (itstool) path: text/tspan
#: elements-f01.svg:106
#, no-wrap
msgid "Principles"
msgstr "Prinzipien"

#. (itstool) path: text/tspan
#: elements-f01.svg:302
#, no-wrap
msgid "Overview"
msgstr "Überblick"

#. (itstool) path: text/tspan
#: elements-f04.svg:79
#, no-wrap
msgid "BIG"
msgstr "GROSS"

#. (itstool) path: text/tspan
#: elements-f04.svg:93
#, no-wrap
msgid "small"
msgstr "klein"

#. (itstool) path: text/tspan
#: elements-f12.svg:710
#, no-wrap
msgid "Random Ant &amp; 4WD"
msgstr "Random Ant &amp; 4WD"

#. (itstool) path: text/tspan
#: elements-f12.svg:721
#, no-wrap
msgid "SVG  Image Created by Andrew Fitzsimon"
msgstr "SVG-Bild gezeichnet von Andrew Fitzsimon"

#. (itstool) path: text/tspan
#: elements-f12.svg:726
#, no-wrap
msgid "Courtesy of Open Clip Art Library"
msgstr "Mit freundlicher Genehmigung der Open Clip Art Library"

#. (itstool) path: text/tspan
#: elements-f12.svg:731
#, no-wrap
msgid "https://openclipart.org/"
msgstr "https://openclipart.org/"

#~ msgid "This is a partial bibliography used to build this document."
#~ msgstr ""
#~ "Dieses unvollständige Literaturverzeichnis wurde zur Erstellung dieses "
#~ "Dokumentes benutzt."

#~ msgid "http://www.makart.com/resources/artclass/EPlist.html"
#~ msgstr "http://www.makart.com/resources/artclass/EPlist.html"

#~ msgid "http://www.princetonol.com/groups/iad/Files/elements2.htm"
#~ msgstr "http://www.princetonol.com/groups/iad/Files/elements2.htm"

#~ msgid "http://www.johnlovett.com/test.htm"
#~ msgstr "http://www.johnlovett.com/test.htm"

#~ msgid "http://digital-web.com/articles/elements_of_design/"
#~ msgstr "http://digital-web.com/articles/elements_of_design/"

#~ msgid "http://digital-web.com/articles/principles_of_design/"
#~ msgstr "http://digital-web.com/articles/principles_of_design/"
