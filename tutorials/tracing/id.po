# translation of Inkscape tutorial tracing to Indonesian
# Copyright (C)
# This file is distributed under the same license as the Inkscape package.
#
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Tracing\n"
"POT-Creation-Date: 2023-07-14 16:46+0200\n"
"PO-Revision-Date: 2010-08-15 10:15+0700\n"
"Last-Translator: @CameliaGirls Translation Team <cameliagirls@googlegroups."
"com>\n"
"Language-Team: Indonesian <id@li.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "pengakuan untuk penerjemah < >, "

#. (itstool) path: articleinfo/title
#: tutorial-tracing.xml:6
#, fuzzy
msgid "Tracing bitmaps"
msgstr "Tracing (Perunutan)"

#. (itstool) path: articleinfo/subtitle
#: tutorial-tracing.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-tracing.xml:11
#, fuzzy
msgid ""
"One of the features in Inkscape is a tool for tracing a bitmap image into "
"one or more &lt;path&gt; elements for your SVG drawing. These short notes "
"should help you become acquainted with how it works."
msgstr ""
"Salah satu fitur dalam Inkscape adalah tool untuk merunut gambar bitmap "
"menjadi elemen &lt;path&gt; untuk menggambar SVG. Note singkat ini bisa "
"membantu anda memahami cara kerjanya."

#. (itstool) path: article/para
#: tutorial-tracing.xml:16
msgid ""
"Keep in mind that the Tracer's purpose is not to reproduce an exact "
"duplicate of the original image; nor is it intended to produce a final "
"product. No autotracer can do that. What it does is give you a set of curves "
"which you can use as a resource for your drawing."
msgstr ""
"Selalu ingat bahwa tujuan dari Perunut bukanlah mereproduksi duplikat eksak "
"dari gambar asal; juga bukanlah untuk menghasilkan produk final. Tidak ada "
"perunut otomatis yang bisa melakukan itu. Apa yang dilakukannya adalah "
"memberikan anda sebuah set kurva yang bisa anda gunakan sebagai bahan "
"menggambar."

#. (itstool) path: article/para
#: tutorial-tracing.xml:21
#, fuzzy
msgid ""
"Our tracer, derived from the original Potrace library by Peter Selinger, "
"interprets a black and white bitmap, and produces a set of curves. For "
"Potrace, we currently have three types of input filters to convert from the "
"raw image to something that Potrace can use."
msgstr ""
"Potrace menerjemahkan lewat bitmap hitam dan putih, kemudian menghasilkan "
"sebuah set kurva. Untuk Potrace, saat ini terdapat tiga tipe filter masukan "
"untuk mengkonversi dari gambar mentah menjadi sesuatu yang bisa digunakan "
"oleh Potrace."

#. (itstool) path: article/para
#: tutorial-tracing.xml:26
msgid ""
"Generally the more dark pixels in the intermediate bitmap, the more tracing "
"that Potrace will perform. As the amount of tracing increases, more CPU time "
"will be required, and the &lt;path&gt; element will become much larger. It "
"is suggested that the user experiment with lighter intermediate images "
"first, getting gradually darker to get the desired proportion and complexity "
"of the output path."
msgstr ""
"Secara umum, semakin banyak pixel gelap dalam sebuah bitmap, semakin banyak "
"runutan yang akan dilakukan oleh Potrace. Semakin besar jumlah runutan, "
"semakin banyak waktu yang dibutuhkan oleh CPU, dan elemennya akan semakin "
"besar. Dianjurkan agar pengguna mencoba dengan gambar yang lebih ringan "
"dahulu, kemudian semakin digelapkan untuk mendapatkan proporsi dan "
"kompleksitisas yang diinginkan."

#. (itstool) path: article/para
#: tutorial-tracing.xml:32
#, fuzzy
msgid ""
"To use the tracer, load or import an image, select it, and select the "
"<menuchoice><guimenu>Path</guimenu><guimenuitem>Trace Bitmap</guimenuitem></"
"menuchoice> item, or <keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"alt\">Alt</keycap><keycap>B</keycap></keycombo>."
msgstr ""
"Untuk menggunakan perunut, load atau import sebuah gambar, seleksi, kemudian "
"pilih <command>Path &gt; Trace Bitmap</command>, atau tekan "
"<keycap>Shift+Alt+B</keycap>."

#. (itstool) path: article/para
#: tutorial-tracing.xml:44
#, fuzzy
msgid "The user will see the five filter options available:"
msgstr "Pengguna akan melihat tiga opsi filter yang ada:"

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:49
#, fuzzy
msgid "Brightness Cutoff"
msgstr "Treshold keterangan"

#. (itstool) path: article/para
#: tutorial-tracing.xml:54
msgid ""
"This merely uses the sum of the red, green and blue (or shades of gray) of a "
"pixel as an indicator of whether it should be considered black or white. The "
"threshold can be set from 0.0 (black) to 1.0 (white). The higher the "
"threshold setting, the fewer the number pixels that will be considered to be "
"“white”, and the intermediate image with become darker."
msgstr ""
"Ini menggunakan nilai dari merah, hijau, dan biru (atau bayangan dari abu-"
"abu) dari sebuah pixel sebagai indikator apakah itu harus dianggap hitam "
"atau putih. Threshold bisa diatur dari 0.0 (hitam) sampai 1.0 (putih). "
"Semakin tinggi threshold, semakin sedikit pixel yang dianggap sebagai "
"“putih“, dan gambar intermediate menjadi gelap."

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:69
#, fuzzy
msgid "Edge Detection"
msgstr "Pendeteksi pinggir"

#. (itstool) path: article/para
#: tutorial-tracing.xml:74
msgid ""
"This uses the edge detection algorithm devised by J. Canny as a way of "
"quickly finding isoclines of similar contrast. This will produce an "
"intermediate bitmap that will look less like the original image than does "
"the result of Brightness Threshold, but will likely provide curve "
"information that would otherwise be ignored. The threshold setting here (0.0 "
"– 1.0) adjusts the brightness threshold of whether a pixel adjacent to a "
"contrast edge will be included in the output. This setting can adjust the "
"darkness or thickness of the edge in the output."
msgstr ""
"Guna dari alogaritma deteksi pinggir oleh J. Canny adalah sebagai cara cepat "
"mencari isoclines dari kontras yang mirip. Ini akan menghasilkan bitmap "
"intermediate yang akan sedikit mirip seperti gambar asal ketimbang hasil "
"dari Brightness Treshold, tetapi akan memberikan informasi kurva yang "
"biasanya tidak diperdulikan. Nilai threshold disini (0.0-1.0) mengatur "
"brightness threshold dari dari apakah pixel yang berkaitan dengan pinggir "
"kontras akan dimasukkan ke hasil. Penataan ini bisa mengatur kegelapan atau "
"ketebalan dari pinggir pada hasil."

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:90
msgid "Color Quantization"
msgstr "Color Quantization (Kuantitasi warna)"

#. (itstool) path: article/para
#: tutorial-tracing.xml:95
msgid ""
"The result of this filter will produce an intermediate image that is very "
"different from the other two, but is very useful indeed. Instead of showing "
"isoclines of brightness or contrast, this will find edges where colors "
"change, even at equal brightness and contrast. The setting here, Number of "
"Colors, decides how many output colors there would be if the intermediate "
"bitmap were in color. It then decides black/white on whether the color has "
"an even or odd index."
msgstr ""
"Hasil dari filter ini adalah gambar intermediate yang sangat berbeda dari "
"sebelumnya, tapi memang sangat berguna. Ketimbang menampilkan isoclines dari "
"brightness (keterangan) atau kontras, ini akan mencari pinggir dimana warna "
"berubah, bahkan pada nilai brightness atau kontras yang sama. Penataan "
"disini, Number of Colors (jumlah warna), menetukan berapa banyak warna hasil "
"jika bitmap intermediate dalam warna. Ia kemudian menentukan hitam/putih "
"pada apakah warna tersebut memiliki index genap atau ganjil."

#. (itstool) path: article/para
#: tutorial-tracing.xml:109
msgid ""
"The user should try all three filters, and observe the different types of "
"output for different types of input images. There will always be an image "
"where one works better than the others."
msgstr ""
"Pengguna sebaiknya mencoba tiga tipe filter tersebut dan mengamati tipe "
"hasil yang berbeda dari tipe masukan yang berbeda. Selalu ada hasil gambar "
"yang lebih baik menggunakan filter yang satu ketimbang yang lain."

#. (itstool) path: article/para
#: tutorial-tracing.xml:113
#, fuzzy
msgid ""
"After tracing, it is also suggested that the user try "
"<menuchoice><guimenu>Path</guimenu><guimenuitem>Simplify</guimenuitem></"
"menuchoice> (<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</"
"keycap></keycombo>) on the output path to reduce the number of nodes. This "
"can make the output of Potrace much easier to edit. For example, here is a "
"typical tracing of the Old Man Playing Guitar:"
msgstr ""
"Setelah merunut, disarankan juga agar pengguna mencoba <command>Path &gt; "
"Simplify</command> (<keycap>Ctrl+L</keycap>) pada path hasil untuk "
"mengurangi jumlah node. Ini bisa membuat hasil dari Potrace lebih mudah "
"diedit. Sebagai contoh, berikut adalah runutan tipikal dari Old Man Playing "
"Guitar:"

#. (itstool) path: article/para
#: tutorial-tracing.xml:127
#, fuzzy
msgid ""
"Note the enormous number of nodes in the path. After hitting "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</keycap></"
"keycombo>, this is a typical result:"
msgstr ""
"Perhatikan jumlah node yang luar biasa didalamnya. Setelah menekan "
"<keycap>Ctrl+L</keycap>, inilah hasilnya:"

#. (itstool) path: article/para
#: tutorial-tracing.xml:138
msgid ""
"The representation is a bit more approximate and rough, but the drawing is "
"much simpler and easier to edit. Keep in mind that what you want is not an "
"exact rendering of the image, but a set of curves that you can use in your "
"drawing."
msgstr ""
"Representasinya sedikit lebih kasar, tetapi gambarnya lebih sederhana dan "
"mudah untuk diedit. Ingatlah bahwa apa yang anda butuhkan bukanlah hasil "
"runutan yang persis seperti gambar yang dirunut, tetapi sebuah set kurva "
"yang bisa anda gunakan dalam gambar."

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:145
msgid "Autotrace"
msgstr ""

#. (itstool) path: article/para
#: tutorial-tracing.xml:150
msgid ""
"The Autotrace option uses a different algorithm for tracing and also offers "
"some other parameters to tweak. It may take a little longer to work, but "
"gives you some variety to choose from."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:156
msgid "Centerline tracing (autotrace)"
msgstr ""

#. (itstool) path: article/para
#: tutorial-tracing.xml:161
msgid ""
"If you would like to vectorize a line drawing, and get strokes that are easy "
"to modify instead of filled areas as a result, use this option. It will "
"attempt to find contiguous lines that make up your drawing."
msgstr ""

#. (itstool) path: Work/format
#: tracing-f01.svg:49 tracing-f02.svg:49 tracing-f03.svg:49 tracing-f04.svg:49
#: tracing-f05.svg:49 tracing-f06.svg:49
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. (itstool) path: text/tspan
#: tracing-f01.svg:70
#, no-wrap
msgid "Main options within the Trace dialog"
msgstr "Opsi utama dalam dialog Trace (Runut)"

#. (itstool) path: text/tspan
#: tracing-f02.svg:70 tracing-f03.svg:70 tracing-f04.svg:70 tracing-f05.svg:70
#: tracing-f06.svg:70
#, no-wrap
msgid "Original Image"
msgstr "Gambar Asal"

#. (itstool) path: text/tspan
#: tracing-f02.svg:81 tracing-f02.svg:97
#, no-wrap
msgid "Brightness Threshold"
msgstr "Treshold keterangan"

#. (itstool) path: text/tspan
#: tracing-f02.svg:86 tracing-f03.svg:86 tracing-f04.svg:86
#, no-wrap
msgid "Fill, no Stroke"
msgstr "Fill (isi), tanpa Stroke (garis pinggir)"

#. (itstool) path: text/tspan
#: tracing-f02.svg:102 tracing-f03.svg:102 tracing-f04.svg:102
#, no-wrap
msgid "Stroke, no Fill"
msgstr "Stroke (garis pinggir) tanpa Fill (isi)"

#. (itstool) path: text/tspan
#: tracing-f03.svg:81 tracing-f03.svg:97
#, no-wrap
msgid "Edge Detected"
msgstr "Pendeteksi pinggir"

#. (itstool) path: text/tspan
#: tracing-f04.svg:81 tracing-f04.svg:97
#, no-wrap
msgid "Quantization (12 colors)"
msgstr "Kuantitas (12 warna)"

#. (itstool) path: text/tspan
#: tracing-f05.svg:81
#, no-wrap
msgid "Traced Image / Output Path"
msgstr "Gambar yang dirunut / Path Keluaran"

#. (itstool) path: text/tspan
#: tracing-f05.svg:86
#, no-wrap
msgid "(1,551 nodes)"
msgstr "(1,551 node)"

#. (itstool) path: text/tspan
#: tracing-f06.svg:81
#, no-wrap
msgid "Traced Image / Output Path - Simplified"
msgstr "Gambar yang dirunut / Path Keluaran - Disederhanakan"

#. (itstool) path: text/tspan
#: tracing-f06.svg:86
#, no-wrap
msgid "(384 nodes)"
msgstr "(384 node)"

#~ msgid ""
#~ "Currently Inkscape employs the Potrace bitmap tracing engine (<ulink "
#~ "url=\"http://potrace.sourceforge.net\">potrace.sourceforge.net</ulink>) "
#~ "by Peter Selinger. In the future we expect to allow alternate tracing "
#~ "programs; for now, however, this fine tool is more than sufficient for "
#~ "our needs."
#~ msgstr ""
#~ "Inkscape saat ini menggunakan jasa mesin perunut gambar Potrace (<ulink "
#~ "url=\"http://potrace.sourceforge.net\">potrace.sourceforge.net</ulink>) "
#~ "oleh Peter Selinger. Di masa depan kamipun berharap ada alternatif lain; "
#~ "untuk sekarang, bagaimanapun juga, tool (alat) ini lebih dari cukup untuk "
#~ "kebutuhan kita."

#~ msgid "Optimal Edge Detection"
#~ msgstr "Optimal Edge Detection (Deteksi pinggir optimal)"
