msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Tracing\n"
"POT-Creation-Date: 2023-07-14 16:46+0200\n"
"PO-Revision-Date: 2009-02-09 11:02+0100\n"
"Last-Translator: Ales Zabala Alava (Shagi) <shagi@gisa-elkartea.org>\n"
"Language-Team: Basque <translation-team-eu@lists.sourceforge.net>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""
"Ales Zabala Alava (Shagi) <shagi@gisa-elkartea.org>, \n"
"Maider Likona Santamarina <pisoni@gisa-elkartea.org>, 2009"

#. (itstool) path: articleinfo/title
#: tutorial-tracing.xml:6
#, fuzzy
msgid "Tracing bitmaps"
msgstr "Bektorizazioa"

#. (itstool) path: articleinfo/subtitle
#: tutorial-tracing.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-tracing.xml:11
#, fuzzy
msgid ""
"One of the features in Inkscape is a tool for tracing a bitmap image into "
"one or more &lt;path&gt; elements for your SVG drawing. These short notes "
"should help you become acquainted with how it works."
msgstr ""
"SVG marrazkietarako bit-mapa irudi bat &lt;bide&gt; elementu batean "
"bektorizatzeko tresna Inkscape-ren ezaugarrietako bat da. Ohar labur hauek "
"bere erabilpenean trebatzen lagundu beharko lizukete."

#. (itstool) path: article/para
#: tutorial-tracing.xml:16
msgid ""
"Keep in mind that the Tracer's purpose is not to reproduce an exact "
"duplicate of the original image; nor is it intended to produce a final "
"product. No autotracer can do that. What it does is give you a set of curves "
"which you can use as a resource for your drawing."
msgstr ""
"Gogoan izan Bektorizatzailearen helburua ez dela jatorrizko irudiaren kopia "
"zehatza lortzea; ezta bukatutako produktua lortzea ere. Ez dago hori lor "
"dezakeen bektorizatzaile automatikorik. Egiten duena zera da, zure "
"marrazkietan baliabide gisa erabil ditzakezun kurba multzoa sortzen du."

#. (itstool) path: article/para
#: tutorial-tracing.xml:21
#, fuzzy
msgid ""
"Our tracer, derived from the original Potrace library by Peter Selinger, "
"interprets a black and white bitmap, and produces a set of curves. For "
"Potrace, we currently have three types of input filters to convert from the "
"raw image to something that Potrace can use."
msgstr ""
"Potrace-k zuri-beltzeko bit-mapa interpretatzen du eta kurba multzo bat "
"sortzen du. Irudi gordin batetik Potrace-k erabil dezakeen zerbaitera "
"heltzeko, gaur egun hiru sarrera iragazki ezberdin ditugu."

#. (itstool) path: article/para
#: tutorial-tracing.xml:26
msgid ""
"Generally the more dark pixels in the intermediate bitmap, the more tracing "
"that Potrace will perform. As the amount of tracing increases, more CPU time "
"will be required, and the &lt;path&gt; element will become much larger. It "
"is suggested that the user experiment with lighter intermediate images "
"first, getting gradually darker to get the desired proportion and complexity "
"of the output path."
msgstr ""
"Orokorrean, gero eta pixel ilun gehiago egon tarteko bit-mapan, Potrace-k "
"bektorizazio gehiago burutuko du. Bektorizatze kopurua gora doan heinean, "
"PUZ gehiago beharko da, eta &lt;bide&gt; elementua askoz handiagoa izango "
"da. Gomendagarria da erabiltzaileak tarteko irudi argiagoekin saiatzea "
"lehenbizi, gradualki ilunagoetara pasatuz helburuko bidearen proportzio eta "
"konplexutasuna lortu arte."

#. (itstool) path: article/para
#: tutorial-tracing.xml:32
#, fuzzy
msgid ""
"To use the tracer, load or import an image, select it, and select the "
"<menuchoice><guimenu>Path</guimenu><guimenuitem>Trace Bitmap</guimenuitem></"
"menuchoice> item, or <keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"alt\">Alt</keycap><keycap>B</keycap></keycombo>."
msgstr ""
"Bektorizatzailea erabiltzeko, irudi bat kargatu edo inportatu, hautatu, eta "
"<command>Bektorizatu bit-mapa</command> elementua aukeratu edo "
"<keycap>Shift+Alt+B</keycap> sakatu."

#. (itstool) path: article/para
#: tutorial-tracing.xml:44
#, fuzzy
msgid "The user will see the five filter options available:"
msgstr "Erabiltzaileak erabilgarri dauden hiru iragazki aukerak ikusiko ditu:"

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:49
#, fuzzy
msgid "Brightness Cutoff"
msgstr "Distira Mozketa"

#. (itstool) path: article/para
#: tutorial-tracing.xml:54
msgid ""
"This merely uses the sum of the red, green and blue (or shades of gray) of a "
"pixel as an indicator of whether it should be considered black or white. The "
"threshold can be set from 0.0 (black) to 1.0 (white). The higher the "
"threshold setting, the fewer the number pixels that will be considered to be "
"“white”, and the intermediate image with become darker."
msgstr ""
"Honek soilik pixel baten gorri, berde eta urdinen (edo grisen itzalen) "
"batura erabiltzen du erabakitzeko beltza edo zuria izan behar duen. Atalasea "
"0,0tik (Beltza) 1.0ra (zuria) ezar daiteke. Gero eta atalase handiagoa, gero "
"eta pixel gutxiago antzemango dira “txuri” bezala, eta bitarteko irudia "
"ilunagoa izango da."

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:69
#, fuzzy
msgid "Edge Detection"
msgstr "Antzemandako Ertza"

#. (itstool) path: article/para
#: tutorial-tracing.xml:74
msgid ""
"This uses the edge detection algorithm devised by J. Canny as a way of "
"quickly finding isoclines of similar contrast. This will produce an "
"intermediate bitmap that will look less like the original image than does "
"the result of Brightness Threshold, but will likely provide curve "
"information that would otherwise be ignored. The threshold setting here (0.0 "
"– 1.0) adjusts the brightness threshold of whether a pixel adjacent to a "
"contrast edge will be included in the output. This setting can adjust the "
"darkness or thickness of the edge in the output."
msgstr ""
"Honek ertz detekziorako J. Canny-k asmatutako algoritmoa erabiltzen du "
"antzeko kontrastea duten isoklinak azkar topatzeko. Honek jatorrizko "
"irudiarekin antzekotasun gutxiago izango duen tarteko bit-mapa sortzen du "
"Distira mozketarekin konparatuta, baina ziur aski beste modura galduko "
"litzatekeen kurben informazioa eskainiko du."

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:90
msgid "Color Quantization"
msgstr "Kolore Kuantizazioa"

#. (itstool) path: article/para
#: tutorial-tracing.xml:95
msgid ""
"The result of this filter will produce an intermediate image that is very "
"different from the other two, but is very useful indeed. Instead of showing "
"isoclines of brightness or contrast, this will find edges where colors "
"change, even at equal brightness and contrast. The setting here, Number of "
"Colors, decides how many output colors there would be if the intermediate "
"bitmap were in color. It then decides black/white on whether the color has "
"an even or odd index."
msgstr ""
"Iragazki honek beste biekin alderatuta tarteko irudi oso diferentea sortuko "
"du, baina oso erabilgarria egia esanda. Distira edo kontrasteko isoklinak "
"erakutsi ordez, kolorea aldatzen duten ertzak bilatuko ditu, nahiz eta "
"distira eta kontraste berdina eduki. Zehaztu daitekeen balioa, Korore "
"kopurua, tarteko bit-mapan egongo diren irteera kopurua adierazten du, "
"tarteko irudia koloretan izatekotan. Ondoren zuri/beltza edo kolorearen "
"indizea bakoiti edo bikoitia den erabakiko du."

#. (itstool) path: article/para
#: tutorial-tracing.xml:109
msgid ""
"The user should try all three filters, and observe the different types of "
"output for different types of input images. There will always be an image "
"where one works better than the others."
msgstr ""
"Erabiltzaileak hiru iragazkiak frogatu beharko lituzke, eta irudi mota "
"ezberdinek sortzen dituzten emaitza ezberdinak aztertu. Beti egongo da irudi "
"bat non iragazki bat besteak baino aproposagoa den."

#. (itstool) path: article/para
#: tutorial-tracing.xml:113
#, fuzzy
msgid ""
"After tracing, it is also suggested that the user try "
"<menuchoice><guimenu>Path</guimenu><guimenuitem>Simplify</guimenuitem></"
"menuchoice> (<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</"
"keycap></keycombo>) on the output path to reduce the number of nodes. This "
"can make the output of Potrace much easier to edit. For example, here is a "
"typical tracing of the Old Man Playing Guitar:"
msgstr ""
"Bektorizatu ondoren, gomendagarria da sortutako bidean <command>Bidea &gt; "
"Soildu</command> (<keycap>Ktrl+L</keycap>) komandoa erabiltzea nodo kopurua "
"txikitzeko. Honek Potrace-ren irteera editatzea asko errazten du. Adibidez, "
"hemen duzu Agurea Gitarra Jotzen-en bektorizazio tipiko bat:"

#. (itstool) path: article/para
#: tutorial-tracing.xml:127
#, fuzzy
msgid ""
"Note the enormous number of nodes in the path. After hitting "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</keycap></"
"keycombo>, this is a typical result:"
msgstr ""
"Ohar zaitez bidean dauden nodo kopuru izugarriaz. <keycap>Ktrl+L</keycap> "
"sakatu eta gero, honelako emaitza lor dezakegu:"

#. (itstool) path: article/para
#: tutorial-tracing.xml:138
msgid ""
"The representation is a bit more approximate and rough, but the drawing is "
"much simpler and easier to edit. Keep in mind that what you want is not an "
"exact rendering of the image, but a set of curves that you can use in your "
"drawing."
msgstr ""
"Irudikapena apur bat zakarragoa eta originarela gutxiago hurbiltzen da, "
"baina marrazkia askoz sinpleagoa eta editatzeko errazagoa da. Gogoan izan "
"zuk nahi duzuna ez dela irudiaren bektorizazio zehatza, baizik eta zure "
"marrazkian erabil ditzakezu kurba multzoa."

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:145
msgid "Autotrace"
msgstr ""

#. (itstool) path: article/para
#: tutorial-tracing.xml:150
msgid ""
"The Autotrace option uses a different algorithm for tracing and also offers "
"some other parameters to tweak. It may take a little longer to work, but "
"gives you some variety to choose from."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:156
msgid "Centerline tracing (autotrace)"
msgstr ""

#. (itstool) path: article/para
#: tutorial-tracing.xml:161
msgid ""
"If you would like to vectorize a line drawing, and get strokes that are easy "
"to modify instead of filled areas as a result, use this option. It will "
"attempt to find contiguous lines that make up your drawing."
msgstr ""

#. (itstool) path: Work/format
#: tracing-f01.svg:49 tracing-f02.svg:49 tracing-f03.svg:49 tracing-f04.svg:49
#: tracing-f05.svg:49 tracing-f06.svg:49
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. (itstool) path: text/tspan
#: tracing-f01.svg:70
#, fuzzy, no-wrap
msgid "Main options within the Trace dialog"
msgstr "Bektorizatu bit-mapa dialogoko aukera nagusiak"

#. (itstool) path: text/tspan
#: tracing-f02.svg:70 tracing-f03.svg:70 tracing-f04.svg:70 tracing-f05.svg:70
#: tracing-f06.svg:70
#, no-wrap
msgid "Original Image"
msgstr "Jatorrizko Irudia"

#. (itstool) path: text/tspan
#: tracing-f02.svg:81 tracing-f02.svg:97
#, no-wrap
msgid "Brightness Threshold"
msgstr "Distira Mozketa"

#. (itstool) path: text/tspan
#: tracing-f02.svg:86 tracing-f03.svg:86 tracing-f04.svg:86
#, no-wrap
msgid "Fill, no Stroke"
msgstr "Betetzea, Trazurik ez"

#. (itstool) path: text/tspan
#: tracing-f02.svg:102 tracing-f03.svg:102 tracing-f04.svg:102
#, no-wrap
msgid "Stroke, no Fill"
msgstr "Trazua, Betetzerik ez"

#. (itstool) path: text/tspan
#: tracing-f03.svg:81 tracing-f03.svg:97
#, no-wrap
msgid "Edge Detected"
msgstr "Antzemandako Ertza"

#. (itstool) path: text/tspan
#: tracing-f04.svg:81 tracing-f04.svg:97
#, no-wrap
msgid "Quantization (12 colors)"
msgstr "Kuantizazioa (12 kolore)"

#. (itstool) path: text/tspan
#: tracing-f05.svg:81
#, no-wrap
msgid "Traced Image / Output Path"
msgstr "Bektorizatutako Irudia / Irteera Bidea"

#. (itstool) path: text/tspan
#: tracing-f05.svg:86
#, no-wrap
msgid "(1,551 nodes)"
msgstr "(1,551 nodo)"

#. (itstool) path: text/tspan
#: tracing-f06.svg:81
#, no-wrap
msgid "Traced Image / Output Path - Simplified"
msgstr "Bektorizatutako Irudia / Irteera Bidea - Sinplifikatuta"

#. (itstool) path: text/tspan
#: tracing-f06.svg:86
#, no-wrap
msgid "(384 nodes)"
msgstr "(384 nodo)"

#~ msgid ""
#~ "Currently Inkscape employs the Potrace bitmap tracing engine (<ulink "
#~ "url=\"http://potrace.sourceforge.net\">potrace.sourceforge.net</ulink>) "
#~ "by Peter Selinger. In the future we expect to allow alternate tracing "
#~ "programs; for now, however, this fine tool is more than sufficient for "
#~ "our needs."
#~ msgstr ""
#~ "Gaur egun Inkscape-k Peter Selinger-en Potrace bit-mapak bektorizatzeko "
#~ "motorea (<ulink url=\"http://potrace.sourceforge.net\">potrace."
#~ "sourceforge.net</ulink>) erabiltzen du. Etorkizunean bektorizatzeko "
#~ "programa ezberdinen artean aukeratzeko gaitasuna izatea espero dugu; "
#~ "oraingoz, ordea, trensa fin hau gure beharretarako nahiko eta soberakoa "
#~ "da."

#~ msgid "Optimal Edge Detection"
#~ msgstr "Ertz Detekzio Optimoa"
