# Translators:
# Somogyvári Róbert <cr04ch at gmail>, 2009.
# Arpad Biro <biro.arpad gmail>, 2009.
# Balázs Meskó <meskobalazs@mailbox.org>, 2023
# Gyuris Gellért <jobel@ujevangelizacio.hu>, 2018, 2019, 2020, 2023
#
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Tracing\n"
"POT-Creation-Date: 2023-07-14 16:46+0200\n"
"PO-Revision-Date: 2023-07-01 20:32+0000\n"
"Last-Translator: Gyuris Gellért <jobel@ujevangelizacio.hu>, 2023\n"
"Language-Team: Hungarian (https://app.transifex.com/fsf-hu/teams/77907/hu/)\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""
"Somogyvári Róbert <cr04ch at gmail>, 2008\n"
"Gyuris Gellért <jobel at ujevangelizacio.hu>, 2018, 2019, 2020, 2021, 2023"

#. (itstool) path: articleinfo/title
#: tutorial-tracing.xml:6
msgid "Tracing bitmaps"
msgstr "Bitkép vektorizálása"

#. (itstool) path: articleinfo/subtitle
#: tutorial-tracing.xml:7
msgid "Tutorial"
msgstr "Ismertető"

#. (itstool) path: abstract/para
#: tutorial-tracing.xml:11
msgid ""
"One of the features in Inkscape is a tool for tracing a bitmap image into "
"one or more &lt;path&gt; elements for your SVG drawing. These short notes "
"should help you become acquainted with how it works."
msgstr ""
"Az Inkscape tartalmaz egy eszközt, amellyel pixelgrafikus képeket lehet egy "
"vagy több útvonallá alakítani. Az alábbi rövid írásból megismerheti ennek az "
"eszköznek a működését."

#. (itstool) path: article/para
#: tutorial-tracing.xml:16
msgid ""
"Keep in mind that the Tracer's purpose is not to reproduce an exact "
"duplicate of the original image; nor is it intended to produce a final "
"product. No autotracer can do that. What it does is give you a set of curves "
"which you can use as a resource for your drawing."
msgstr ""
"Vegye figyelembe, hogy a vektorizálás célja nem egy kép pontos másolatának a "
"létrehozása; mint ahogy nem célja a végső eredmény előállítása sem. Egyik "
"vektorizáló program sem nyújt ilyesmit. Ezzel szemben olyan görbékhez "
"juthat, melyeket aztán a rajzához kiindulásként használhat."

#. (itstool) path: article/para
#: tutorial-tracing.xml:21
msgid ""
"Our tracer, derived from the original Potrace library by Peter Selinger, "
"interprets a black and white bitmap, and produces a set of curves. For "
"Potrace, we currently have three types of input filters to convert from the "
"raw image to something that Potrace can use."
msgstr ""
"A Peter Selinger eredeti Potrace könyvtárából származó vektorizáló eszköz "
"fekete-fehér bitképeket dolgoz fel, és görbéket állít elő. Jelenleg az "
"Inkscape háromfajta bemeneti szűrőt biztosít, melyekkel egy bitkép a Potrace "
"számára feldolgozhatóvá válik."

#. (itstool) path: article/para
#: tutorial-tracing.xml:26
msgid ""
"Generally the more dark pixels in the intermediate bitmap, the more tracing "
"that Potrace will perform. As the amount of tracing increases, more CPU time "
"will be required, and the &lt;path&gt; element will become much larger. It "
"is suggested that the user experiment with lighter intermediate images "
"first, getting gradually darker to get the desired proportion and complexity "
"of the output path."
msgstr ""
"Általában minél több sötét pixel van a közbeeső képen, a Potrace annál több "
"átrajzolást fog végezni. Minél több az átrajzolás, annál több "
"processzoridőre lesz szükség, és az útvonal annál összetettebb lesz. "
"Ajánlott, hogy előbb világosabb közbenső képekkel kísérletezzen, és "
"fokozatos sötétítéssel jusson el a megfelelő összhanghoz és összetettséghez."

#. (itstool) path: article/para
#: tutorial-tracing.xml:32
msgid ""
"To use the tracer, load or import an image, select it, and select the "
"<menuchoice><guimenu>Path</guimenu><guimenuitem>Trace Bitmap</guimenuitem></"
"menuchoice> item, or <keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"alt\">Alt</keycap><keycap>B</keycap></keycombo>."
msgstr ""
"A vektorizáláshoz először nyisson meg vagy importáljon egy képet, jelölje "
"ki, majd válassza az <menuchoice><guimenu>Útvonal</guimenu><guimenuitem> "
"Bitkép vektorizálása…</guimenuitem></menuchoice> parancsot vagy nyomja meg a "
"<keycombo><keycap function=\"shift\">Shift</keycap><keycap "
"function=\"alt\">Alt</keycap><keycap>B</keycap></keycombo> billentyűket."

#. (itstool) path: article/para
#: tutorial-tracing.xml:44
msgid "The user will see the five filter options available:"
msgstr "A párbeszédablakban az öt szűrő beállításai láthatók:"

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:49
msgid "Brightness Cutoff"
msgstr "Fényesség-levágás"

#. (itstool) path: article/para
#: tutorial-tracing.xml:54
msgid ""
"This merely uses the sum of the red, green and blue (or shades of gray) of a "
"pixel as an indicator of whether it should be considered black or white. The "
"threshold can be set from 0.0 (black) to 1.0 (white). The higher the "
"threshold setting, the fewer the number pixels that will be considered to be "
"“white”, and the intermediate image with become darker."
msgstr ""
"Ez a szűrő pusztán a vörös, a zöld és a kék (vagy a szürke árnyalatai) "
"színek összege alapján dönti el egy pixelről, hogy fehér vagy fekete legyen-"
"e. A küszöb 0,0 (fekete) és 1,0 (fehér) között lehet. A küszöb növelésével "
"csökken a fehérként figyelembe veendő pixelek száma, így a közbenső kép "
"sötétebb lesz."

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:69
msgid "Edge Detection"
msgstr "Élkeresés"

#. (itstool) path: article/para
#: tutorial-tracing.xml:74
msgid ""
"This uses the edge detection algorithm devised by J. Canny as a way of "
"quickly finding isoclines of similar contrast. This will produce an "
"intermediate bitmap that will look less like the original image than does "
"the result of Brightness Threshold, but will likely provide curve "
"information that would otherwise be ignored. The threshold setting here (0.0 "
"– 1.0) adjusts the brightness threshold of whether a pixel adjacent to a "
"contrast edge will be included in the output. This setting can adjust the "
"darkness or thickness of the edge in the output."
msgstr ""
"J. Canny élkeresési algoritmusára épülő szűrő, mellyel gyorsan "
"elválaszthatók a hasonló kontrasztú területek. A közbenső kép itt az előző "
"szűrőhöz képest kevésbé részletes, viszont ezáltal olyan görbék is "
"nyerhetők, amilyenek másképpen nem. A küszöb (0,0–1,0) ezúttal egy "
"fényességi küszöböt jelent, amely alapján egy kontrasztos éllel szomszédos "
"pixelek bekerülnek a kimenetbe. Ezzel szabályozható a kimenetben az él "
"sötétsége vagy vastagsága."

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:90
msgid "Color Quantization"
msgstr "Színek számának csökkentése"

#. (itstool) path: article/para
#: tutorial-tracing.xml:95
msgid ""
"The result of this filter will produce an intermediate image that is very "
"different from the other two, but is very useful indeed. Instead of showing "
"isoclines of brightness or contrast, this will find edges where colors "
"change, even at equal brightness and contrast. The setting here, Number of "
"Colors, decides how many output colors there would be if the intermediate "
"bitmap were in color. It then decides black/white on whether the color has "
"an even or odd index."
msgstr ""
"Az előzőektől nagyon eltérő közbenső képet eredményező, ám mégis nagyon "
"hasznos szűrő. A fényességeltérések vagy a kontrasztok helyett a színek "
"változásai mentén keresi az éleket, akár egyenlő fényesség vagy kontraszt "
"mellett is. A színek számával az határozható meg, hogy a kimenetben hány "
"szín lenne, ha a közbenső kép színes volna. Ezután a szűrő az alapján dönti "
"el a fekete-fehér kérdést, hogy egy-egy szín páros vagy páratlan indexű-e."

#. (itstool) path: article/para
#: tutorial-tracing.xml:109
msgid ""
"The user should try all three filters, and observe the different types of "
"output for different types of input images. There will always be an image "
"where one works better than the others."
msgstr ""
"Célszerű kipróbálni mindhárom szűrőt, és megfigyelni a különféle típusú "
"bemeneti képekkel kapott útvonalak közti különbségeket. Mindig van olyan "
"kép, amelynek esetében az egyik szűrő a többinél jobb eredményt ad."

#. (itstool) path: article/para
#: tutorial-tracing.xml:113
msgid ""
"After tracing, it is also suggested that the user try "
"<menuchoice><guimenu>Path</guimenu><guimenuitem>Simplify</guimenuitem></"
"menuchoice> (<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</"
"keycap></keycombo>) on the output path to reduce the number of nodes. This "
"can make the output of Potrace much easier to edit. For example, here is a "
"typical tracing of the Old Man Playing Guitar:"
msgstr ""
"Vektorizálás után ajánlott az eredményt az <menuchoice><guimenu>Útvonal </"
"guimenu><guimenuitem>Egyszerűsítés</guimenuitem></menuchoice> "
"(<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</keycap></"
"keycombo>) paranccsal feldolgozni, a csomópontokat csökkentendő. Ez "
"könnyebben szerkeszthetővé is teszi a Potrace kimenetét. Példaként álljon "
"itt az Old Man Playing Guitar című kép vektorizálása:"

#. (itstool) path: article/para
#: tutorial-tracing.xml:127
msgid ""
"Note the enormous number of nodes in the path. After hitting "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</keycap></"
"keycombo>, this is a typical result:"
msgstr ""
"Figyelje meg, hogy milyen sok csomópontot tartalmaz az útvonal. És a "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</keycap></"
"keycombo> megnyomása utáni tipikus eredmény:"

#. (itstool) path: article/para
#: tutorial-tracing.xml:138
msgid ""
"The representation is a bit more approximate and rough, but the drawing is "
"much simpler and easier to edit. Keep in mind that what you want is not an "
"exact rendering of the image, but a set of curves that you can use in your "
"drawing."
msgstr ""
"A kép így kicsit elnagyoltabb lett, de sokkal egyszerűbbé is vált, ezért "
"könnyebb szerkeszteni. Ne feledje, hogy nem a kép pontos visszaadása volt a "
"cél, hanem olyan görbékhez jutni, melyek felhasználhatók lesznek egy rajzban."

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:145
msgid "Autotrace"
msgstr "Autotrace"

#. (itstool) path: article/para
#: tutorial-tracing.xml:150
msgid ""
"The Autotrace option uses a different algorithm for tracing and also offers "
"some other parameters to tweak. It may take a little longer to work, but "
"gives you some variety to choose from."
msgstr ""
"Az Autotrace opció más algoritmust használ a vektorizáláshoz, és néhány más "
"paramétert is kínál a beállításokhoz. Lehet, hogy egy kicsit tovább tart, "
"amíg működik, de ad némi választékot."

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:156
msgid "Centerline tracing (autotrace)"
msgstr "Középvonalas vektorizálás (autotrace)"

#. (itstool) path: article/para
#: tutorial-tracing.xml:161
msgid ""
"If you would like to vectorize a line drawing, and get strokes that are easy "
"to modify instead of filled areas as a result, use this option. It will "
"attempt to find contiguous lines that make up your drawing."
msgstr ""
"Ha egy vonalas rajzot szeretne vektorizálni, és ennek eredményeképpen "
"kitöltött területek helyett könnyen módosítható vonalakat szeretne kapni, "
"használja ezt az opciót, ugyanis ez a módszer megpróbálja megtalálni a "
"rajzot alkotó összefüggő vonalakat."

#. (itstool) path: Work/format
#: tracing-f01.svg:49 tracing-f02.svg:49 tracing-f03.svg:49 tracing-f04.svg:49
#: tracing-f05.svg:49 tracing-f06.svg:49
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. (itstool) path: text/tspan
#: tracing-f01.svg:70
#, no-wrap
msgid "Main options within the Trace dialog"
msgstr "A Bitkép vektorizálása párbeszédablak"

#. (itstool) path: text/tspan
#: tracing-f02.svg:70 tracing-f03.svg:70 tracing-f04.svg:70 tracing-f05.svg:70
#: tracing-f06.svg:70
#, no-wrap
msgid "Original Image"
msgstr "Eredeti kép"

#. (itstool) path: text/tspan
#: tracing-f02.svg:81 tracing-f02.svg:97
#, no-wrap
msgid "Brightness Threshold"
msgstr "Fényesség-levágás"

#. (itstool) path: text/tspan
#: tracing-f02.svg:86 tracing-f03.svg:86 tracing-f04.svg:86
#, no-wrap
msgid "Fill, no Stroke"
msgstr "csak kitöltéssel"

#. (itstool) path: text/tspan
#: tracing-f02.svg:102 tracing-f03.svg:102 tracing-f04.svg:102
#, no-wrap
msgid "Stroke, no Fill"
msgstr "csak körvonallal"

#. (itstool) path: text/tspan
#: tracing-f03.svg:81 tracing-f03.svg:97
#, no-wrap
msgid "Edge Detected"
msgstr "Élkeresés"

#. (itstool) path: text/tspan
#: tracing-f04.svg:81 tracing-f04.svg:97
#, no-wrap
msgid "Quantization (12 colors)"
msgstr "Színek csökkentése (12)"

#. (itstool) path: text/tspan
#: tracing-f05.svg:81
#, no-wrap
msgid "Traced Image / Output Path"
msgstr "A vektorizálás eredménye"

#. (itstool) path: text/tspan
#: tracing-f05.svg:86
#, no-wrap
msgid "(1,551 nodes)"
msgstr "(1551 csomópont)"

#. (itstool) path: text/tspan
#: tracing-f06.svg:81
#, no-wrap
msgid "Traced Image / Output Path - Simplified"
msgstr "A vektorizálás eredménye egyszerűsítés után"

#. (itstool) path: text/tspan
#: tracing-f06.svg:86
#, no-wrap
msgid "(384 nodes)"
msgstr "(384 csomópont)"
