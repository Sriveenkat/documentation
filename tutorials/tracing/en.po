msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Tracing\n"
"POT-Creation-Date: 2023-07-14 16:46+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""

#. (itstool) path: articleinfo/title
#: tutorial-tracing.xml:6
msgid "Tracing bitmaps"
msgstr ""

#. (itstool) path: articleinfo/subtitle
#: tutorial-tracing.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-tracing.xml:11
msgid ""
"One of the features in Inkscape is a tool for tracing a bitmap image into "
"one or more &lt;path&gt; elements for your SVG drawing. These short notes "
"should help you become acquainted with how it works."
msgstr ""

#. (itstool) path: article/para
#: tutorial-tracing.xml:16
msgid ""
"Keep in mind that the Tracer's purpose is not to reproduce an exact "
"duplicate of the original image; nor is it intended to produce a final "
"product. No autotracer can do that. What it does is give you a set of curves "
"which you can use as a resource for your drawing."
msgstr ""

#. (itstool) path: article/para
#: tutorial-tracing.xml:21
msgid ""
"Our tracer, derived from the original Potrace library by Peter Selinger, "
"interprets a black and white bitmap, and produces a set of curves. For "
"Potrace, we currently have three types of input filters to convert from the "
"raw image to something that Potrace can use."
msgstr ""

#. (itstool) path: article/para
#: tutorial-tracing.xml:26
msgid ""
"Generally the more dark pixels in the intermediate bitmap, the more tracing "
"that Potrace will perform. As the amount of tracing increases, more CPU time "
"will be required, and the &lt;path&gt; element will become much larger. It "
"is suggested that the user experiment with lighter intermediate images "
"first, getting gradually darker to get the desired proportion and complexity "
"of the output path."
msgstr ""

#. (itstool) path: article/para
#: tutorial-tracing.xml:32
msgid ""
"To use the tracer, load or import an image, select it, and select the "
"<menuchoice><guimenu>Path</guimenu><guimenuitem>Trace Bitmap</guimenuitem></"
"menuchoice> item, or <keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"alt\">Alt</keycap><keycap>B</keycap></keycombo>."
msgstr ""

#. (itstool) path: article/para
#: tutorial-tracing.xml:44
msgid "The user will see the five filter options available:"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:49
msgid "Brightness Cutoff"
msgstr ""

#. (itstool) path: article/para
#: tutorial-tracing.xml:54
msgid ""
"This merely uses the sum of the red, green and blue (or shades of gray) of a "
"pixel as an indicator of whether it should be considered black or white. The "
"threshold can be set from 0.0 (black) to 1.0 (white). The higher the "
"threshold setting, the fewer the number pixels that will be considered to be "
"“white”, and the intermediate image with become darker."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:69
msgid "Edge Detection"
msgstr ""

#. (itstool) path: article/para
#: tutorial-tracing.xml:74
msgid ""
"This uses the edge detection algorithm devised by J. Canny as a way of "
"quickly finding isoclines of similar contrast. This will produce an "
"intermediate bitmap that will look less like the original image than does "
"the result of Brightness Threshold, but will likely provide curve "
"information that would otherwise be ignored. The threshold setting here (0.0 "
"– 1.0) adjusts the brightness threshold of whether a pixel adjacent to a "
"contrast edge will be included in the output. This setting can adjust the "
"darkness or thickness of the edge in the output."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:90
msgid "Color Quantization"
msgstr ""

#. (itstool) path: article/para
#: tutorial-tracing.xml:95
msgid ""
"The result of this filter will produce an intermediate image that is very "
"different from the other two, but is very useful indeed. Instead of showing "
"isoclines of brightness or contrast, this will find edges where colors "
"change, even at equal brightness and contrast. The setting here, Number of "
"Colors, decides how many output colors there would be if the intermediate "
"bitmap were in color. It then decides black/white on whether the color has "
"an even or odd index."
msgstr ""

#. (itstool) path: article/para
#: tutorial-tracing.xml:109
msgid ""
"The user should try all three filters, and observe the different types of "
"output for different types of input images. There will always be an image "
"where one works better than the others."
msgstr ""

#. (itstool) path: article/para
#: tutorial-tracing.xml:113
msgid ""
"After tracing, it is also suggested that the user try "
"<menuchoice><guimenu>Path</guimenu><guimenuitem>Simplify</guimenuitem></"
"menuchoice> (<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</"
"keycap></keycombo>) on the output path to reduce the number of nodes. This "
"can make the output of Potrace much easier to edit. For example, here is a "
"typical tracing of the Old Man Playing Guitar:"
msgstr ""

#. (itstool) path: article/para
#: tutorial-tracing.xml:127
msgid ""
"Note the enormous number of nodes in the path. After hitting "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</keycap></"
"keycombo>, this is a typical result:"
msgstr ""

#. (itstool) path: article/para
#: tutorial-tracing.xml:138
msgid ""
"The representation is a bit more approximate and rough, but the drawing is "
"much simpler and easier to edit. Keep in mind that what you want is not an "
"exact rendering of the image, but a set of curves that you can use in your "
"drawing."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:145
msgid "Autotrace"
msgstr ""

#. (itstool) path: article/para
#: tutorial-tracing.xml:150
msgid ""
"The Autotrace option uses a different algorithm for tracing and also offers "
"some other parameters to tweak. It may take a little longer to work, but "
"gives you some variety to choose from."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:156
msgid "Centerline tracing (autotrace)"
msgstr ""

#. (itstool) path: article/para
#: tutorial-tracing.xml:161
msgid ""
"If you would like to vectorize a line drawing, and get strokes that are easy "
"to modify instead of filled areas as a result, use this option. It will "
"attempt to find contiguous lines that make up your drawing."
msgstr ""

#. (itstool) path: Work/format
#: tracing-f01.svg:49 tracing-f02.svg:49 tracing-f03.svg:49 tracing-f04.svg:49
#: tracing-f05.svg:49 tracing-f06.svg:49
msgid "image/svg+xml"
msgstr ""

#. (itstool) path: text/tspan
#: tracing-f01.svg:70
#, no-wrap
msgid "Main options within the Trace dialog"
msgstr ""

#. (itstool) path: text/tspan
#: tracing-f02.svg:70 tracing-f03.svg:70 tracing-f04.svg:70 tracing-f05.svg:70
#: tracing-f06.svg:70
#, no-wrap
msgid "Original Image"
msgstr ""

#. (itstool) path: text/tspan
#: tracing-f02.svg:81 tracing-f02.svg:97
#, no-wrap
msgid "Brightness Threshold"
msgstr ""

#. (itstool) path: text/tspan
#: tracing-f02.svg:86 tracing-f03.svg:86 tracing-f04.svg:86
#, no-wrap
msgid "Fill, no Stroke"
msgstr ""

#. (itstool) path: text/tspan
#: tracing-f02.svg:102 tracing-f03.svg:102 tracing-f04.svg:102
#, no-wrap
msgid "Stroke, no Fill"
msgstr ""

#. (itstool) path: text/tspan
#: tracing-f03.svg:81 tracing-f03.svg:97
#, no-wrap
msgid "Edge Detected"
msgstr ""

#. (itstool) path: text/tspan
#: tracing-f04.svg:81 tracing-f04.svg:97
#, no-wrap
msgid "Quantization (12 colors)"
msgstr ""

#. (itstool) path: text/tspan
#: tracing-f05.svg:81
#, no-wrap
msgid "Traced Image / Output Path"
msgstr ""

#. (itstool) path: text/tspan
#: tracing-f05.svg:86
#, no-wrap
msgid "(1,551 nodes)"
msgstr ""

#. (itstool) path: text/tspan
#: tracing-f06.svg:81
#, no-wrap
msgid "Traced Image / Output Path - Simplified"
msgstr ""

#. (itstool) path: text/tspan
#: tracing-f06.svg:86
#, no-wrap
msgid "(384 nodes)"
msgstr ""
